<?php
/**
 * Config
 *
 * Define all public configuration variables used in the application.
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 *
 */
class Config {

    /*
     * PAGE SETTINGS
     */
    // Admin email for site notifications
    public static $admin_email  = 'email@example.com';
    // Domain of the page (can contain subdomains or subdirectories)
    public static $page_domain  = 'example.net';
    // Address used to send email
    public static $from_email   = 'noreply@example.net';
    // Prefix to use for email subjects
    public static $subject_prefix = '[TravelExpenses]';

    // HTML Page Description
    public static $page_description = 'Travel Expense Manager';
    // Which image should be used as favicon
    public static $favicon          = 'images/logo.png';
    // The name of the website (used in the title)
    public static $sitename         = 'Travel Expense Manager';


    /*
     * DATABASE SETTINGS
     */
    // The host you want to connect to.
    public static $db_host      = 'database.example.net';
    // The database name.
    public static $db_name      = '239487';
    // The database username.
    public static $db_user      = '239487';
    // The database password.
    public static $db_password  = '81239412304';


    /*
     * USER SETTINGS
     */
    // currently not used
    public static $can_register = 'any';
    // currently not used
    public static $default_role = 'member';


    /*
     * SECURITY SETTINGS
     */
    // Can the site be found by search engines? (true, false)
    public static $search_engine    = false;
    // Is the cookie sent over a secure connection? (true, false)
    public static $secure           = false;
    // Should Cronjobs be executed?
    public static $cron_active      = true;


    /*
     * MENU SETTINGS
     *
     * All pages must be listed here. To add a page to the menu you can also add
     * 'link' (displayed in menu) and 'side' (on which side of the menu bar does
     * the link appear) attributes.
     * Possible values for 'login' are 0 (everyone), 1 (logged in users), 2 (only admin)
     */
    public static $menu =
        [['name'          => 'mytrips',
            'title'       => 'My Trips',
            'link'        => 'My Trips',
            'side'        => 'left',
            'login'       => 2,
            'stylesheets' => ['rides'],
            'js_bottom'   => []
        ],

        ['name'          => 'mydeposits',
            'title'       => 'My Deposits',
            'link'        => 'My Deposits',
            'side'        => 'left',
            'login'       => 2,
            'stylesheets' => ['rides'],
            'js_bottom'   => []
        ],

        ['name'           => 'addtrip',
            'title'       => 'Add Trip',
            'link'        => 'Add Trip',
            'side'        => 'none',
            'login'       => 2,
            'stylesheets' => ['rides'],
            'js_bottom'   => []
        ],

        ['name'           => 'allmembers',
            'title'       => 'All Members',
            'link'        => 'All Members',
            'side'        => 'right',
            'login'       => 3,
            'stylesheets' => ['rides'],
            'js_bottom'   => []
        ],

        ['name'           => 'admin',
            'title'       => 'Admin',
            'link'        => 'Admin',
            'side'        => 'right',
            'login'       => 3,
            'stylesheets' => ['rides'],
            'js_bottom'   => ['sha512']
        ],

        ['name'           => 'profile',
            'title'       => 'Profile',
            'link'        => 'Profile',
            'side'        => 'right',
            'login'       => 2,
            'stylesheets' => ['rides'],
            'js_bottom'   => ['sha512']
        ],

        ['name'           => 'register',
            'title'       => 'Register',
            'login'       => 0,
            'stylesheets' => ['signin'],
            'js_bottom'   => ['sha512']
        ],

        ['name'           => 'forgot-password',
            'title'       => 'Forgot Passwort',
            'login'       => 0,
            'stylesheets' => ['signin'],
            'js_bottom'   => []
        ],

        ['name'           => 'reset-password',
            'handler'     => 'User',
            'title'       => 'Reset Password',
            'login'       => 0,
            'stylesheets' => ['signin'],
            'js_bottom'   => ['sha512']
        ],

        ['name'           => 'login',
            'title'       => 'Login',
            'login'       => 0,
            'stylesheets' => ['signin'],
            'js_bottom'   => ['sha512']
        ]];
}
