<?php
/**
 * Index
 *
 * Initialize the application, start the output buffer and flush it afterwards
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */

// Define constant so that other files can only be executed after this file.
define('_MAINEXEC', 1);

ob_start(); // Start output buffer

// Load Application and instantiate
require_once 'library/application.php';
require_once 'Config.php';
$app = new library\Application();

// Create Session
$session = new library\Session();

// Initialize the application and perform an action/show a view
$load = $app->initialize($session->checkLoginStatus());
$load->perform($session);

ob_end_flush(); // Flush the output buffer
