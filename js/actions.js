/*!
 * actions.js
 *
 * Adds the following functionality to documents:
 *  -  Submitting forms and handling json feedback to display alerts and/or
 *     manipulate page content.
 *  -  Checking password input for registration (making sure passwords are
 *     equal and comply to given rules).
 *  -  Converting passwords to sha512 hashes for submission making it less
 *     likely that important passwords can be figured out.
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch
 */


/**
 * submitForm
 *
 * Performs an ajax request using the given url and form handle. Afterwards
 * it handles the json feedback from the server to display alerts or manipulate
 * data. The return data from the server should be a json encoded array of data.
 *
 * @param {string} action_url   Url of the server side action handler
 * @param {Jquery} form_handle  Jquery object of the form to be processed
 * @returns {undefined}
 */
function submitForm(action_url, form_handle) {
    // Perform ajax request
    $.ajax({
        url:  action_url,
        type: 'POST',
        data: form_handle.serialize(),
        datatype: 'json',
        success: function(data)
        {
            if (data['do'] === 'redirect') {
                // If the requested action is a redirect
                window.location.href = data['target'];
            } else if (data['do'] === 'display') {
                // If there is data to display
                if (data['success'] === 'success') {
                    // Server returns a successful response
                    showSuccess(data['message']);

                    // Disable button for a short time to prevent multiple submissions
                    form_handle.find('button').each(function() {
                        var button = $(this);
                        $(this).attr('disabled', true);
                        window.setTimeout(function(){
                            button.attr('disabled', false);
                            }, 500
                        );
                    });

                    // If submission was successful maybe there is some other data
                    // to be updated
                    // Works only if there is only one table...
                    if (typeof data['update_table'] !== 'undefined') {
                        if (data['update_table'] === 'delete_row') {
                            // delete a specified row from the table in the document
                            $('tr#' + data['update_tr']).remove();
                        }
                        if (data['update_table'] === 'update_cell') {
                            // update a specified cell in the table
                            $('tr#' + data['cell']['row'] + '>td:nth-child(' + data['cell']['col'] + ')')
                                    .text(data['data']);
                        }
                    }

                } else {
                    // server returned an error, show that
                    showAlert(data['message']);
                }
            } else {
                // The server response didn't contain info we can work with
                // so here is just a generic error
                showAlert('Sorry! Something went wrong (Unknown Error).');
            }
        },
        error: function(data)
        {
            // The request failed so a generic error is shown
            showAlert('Sorry! Something went wrong (Unknown Error).');
        }
    });
}

/**
 * showAlert
 *
 * Needs a div with class .error to insert an .alert-danger message inside. The
 * message is formatted to be automatically dismissable.
 *
 * @param {string} message Message to be displayed
 * @returns {undefined}
 */
function showAlert(message) {
    // HTML for the alert box and a close button
    var messageBox = "<div class='alert alert-danger alert-dismissible fade in' >\n\
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>\n\
                                <span aria-hidden='true'>&times;</span>\n\
                            </button>\n\
                            <p class='error'></p>\n\
                        <div>";
    $('div.error').html(messageBox);        // Insert the message box
    $('p.error').html(message);             // Then insert the message text inside it
    $('div.error').show();                  // Make sure the box is shown
}

/**
 * showSuccess
 *
 * Needs a div with class.error to insert an .alert-success message inside. The
 * message is formatted to be automatically dismissible.
 *
 * @param {string} message Message to be displayed
 * @returns {undefined}
 */
function showSuccess(message) {
    // HTML for the success box and a close button
    var messageBox = "<div class='alert alert-success alert-dismissible fade in' >\n\
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>\n\
                            <span aria-hidden='true'>&times;</span>\n\
                        </button>\n\
                        <p class='text-success'></p>\n\
                    <div>";
    $('div.error').html(messageBox);        // Insert the message box
    $('p.text-success').html(message);      // Insert the message inside it
    $('div.error').show();                  // Make sure the box is shown
}

/**
 * clearAlert
 *
 * Closes an existing alert or success message.
 *
 * @returns {undefined}
 */
function clearAlert() {
    $('.alert').alert('close');             // Perform close event of the alert class
}

/**
 * showFormError
 *
 * Adds an error style to a specified form field and displays the
 * given alert message.
 *
 * @param {JQuery} field    JQuery input field object
 * @param {string} message  The message to be displayed
 * @returns {undefined}
 */
function showFormError(field, message) {
    // Remove the success class from the input field
    field.parent().removeClass('has-success has-feedback');
    field.next('.glyphicon').removeClass('glyphicon-ok');

    // Add the error classes to the input field
    field.next('.glyphicon').show();
    field.parent().addClass('has-error has-feedback');
    field.next('.glyphicon').addClass('glyphicon-remove');

    // Only show the alert box if there is a message for it
    message = typeof message !== 'undefined' ? message : '';
    if (message !== '') {
        showAlert(message);
    };
}

/**
 * showFormSuccess
 *
 * Adds a success style to a specified form field and removes the message box.
 *
 * @param {JQuery} field    JQuery input field object
 * @returns {undefined}
 */
function showFormSuccess(field) {
    // Remove error classes from input field
    field.parent().removeClass('has-error has-feedback');
    field.next('.glyphicon').removeClass('glyphicon-remove');

    // Add success classes
    field.next('.glyphicon').show();
    field.parent().addClass('has-success has-feedback');
    field.next('.glyphicon').addClass('glyphicon-ok');

    // Clear the alert message (if it exists)
    clearAlert();
}

/**
 * checkPW
 *
 * Takes two input fields and compares their values. The string in the first
 * field must comply to the given regex for secure passwords and the value of
 * the second input field must equal the value of the first field.
 *
 * @param {JQuery} firstCheck   Jquery object of the first input field
 * @param {JQuery} seconCheck   JQuery object of the second input field
 * @returns {undefined}
 */
function checkPW(firstCheck, seconCheck) {
    var aerror = false;
    var berror = false;

    if (firstCheck.val() !== '') {
        //If the first field contains something
        // Setup the regex (min 6 characters, 1 capital, 1 lowercase letter and 2 digits)
        var regex = /^(?=.{6,}$)(?=(.*[A-Z]){1})(?=(.*[a-z]){1})(?=(.*[0-9]){2})(?=[\S ]+$).*$/g;
        if (regex.test(firstCheck.val()) === false) {
            // The first field must comply to the regex
            aerror = true;
        }
        if (firstCheck.val() !== seconCheck.val()) {
            // The second field must be equal to the first one
            berror = true;
        }

        // Show some errors if the rules were not followed
        if (aerror === true) {
            showFormError(firstCheck, 'Password must conform to requirements. (Min. 6 characters, min. 2 numbers, some capitals.)');
        } else {
            showFormSuccess(firstCheck);
        }
        if (berror === true) {
            showFormError(seconCheck, '');
        } else {
            showFormSuccess(seconCheck);
        }
    } else {
        // If the fields are empty remove all feedback
        firstCheck.parent().removeClass('has-error has-feedback');
        firstCheck.parent().removeClass('has-success has-feedback');
        firstCheck.next('.glyphicon').hide();
        seconCheck.parent().removeClass('has-error has-feedback');
        seconCheck.parent().removeClass('has-success has-feedback');
        seconCheck.next('.glyphicon').hide();
        clearAlert();
    }
}

/*
 * Prepare, add event handlers and format the document after it is completely
 * loaded.
 */
$( document ).ready(function() {

    // If the user clicks on a button with name, value pair attributes, an input
    // field is added to make sure the information is transmitted through the
    // ajax request.
    $(document).on('click', '[name][value]:button', function(evt){
        var $button = $(evt.currentTarget),
            // If there is an input with the same name use that for the value
            $input = $button.closest('form').find('input[name="'+$button.attr('name')+'"]');
        if(!$input.length){
            // If there is no input field yet create one
            $input = $('<input>', {
                type:'hidden',
                name:$button.attr('name'),
                class:'button-value-input'
            });
            $input.insertAfter($button);
        }
        // Insert the button value into the input field
        $input.val($button.val());
    });

    // Add form submission event handler
    $('form').submit(function(e){
        e.preventDefault();         // Don't do the default form action

        // If the form has class .confirm-pw, look for input fields with type
        // password. If those fields have an error display an appropriate error
        // message.
        if ($(this).hasClass('confirm-pw') &&
            $(this).find('input[type="password"]').parent().hasClass('has-error')) {
            showAlert('Passwords must be equal!');
            return;
        }

        // Convert password inputs into a sha512 hash for submission
        $(this).find('input[type="password"]').each(function() {
            if ($(this).val() !== ''){
                $(this).val(hex_sha512($(this).val()));
            }
        });

        // Find select fields with the .select-search class. They have the chosen
        // search field attached to them and we need to combine the values into
        // a separate input field for ajax submission
        var $thisform = $(this);
        $(this).find('select.select-search').each(function(){
            $('<input>').attr({
                type: 'hidden',
                name: $(this).attr('name') + '_chosen',
                class: 'select-search-value',
                value: $(this).chosen().val()
            }).appendTo($thisform);
        });

        // Figure out the action url and submit the form
        var url = $(this).attr('action');
        submitForm(url, $(this));

        // Remove all the additional input fields that were added for submission
        // and clear password fields. This allows the same form to be submitted
        // again without interference
        $(this).find('.select-search-value').each(function() {
            $(this).remove();
        });
        $(this).find('.button-value-input').each(function() {
            $(this).remove();
        });
        $(this).find('input[type="password"]').each(function() {
            $(this).val('');
        });
    });

    // Attach the checkPW function as an event handler for password fields.
    // They check the input every time a letter is entered
    var firstCheck = $('form.confirm-pw').find("input[type='password']").eq(0);
    var seconCheck = $('form.confirm-pw').find("input[type='password']").eq(1);
    firstCheck.keyup(function(){checkPW(firstCheck, seconCheck);});
    seconCheck.keyup(function(){checkPW(firstCheck, seconCheck);});

    // Attach the chosen handler to select fields with class .select-search
    $('select.select-search').chosen({
        width: '98%',
        max_shown_results: 25,
        display_selected_options: false,
        no_results_text: 'Keine Übereinstimmungen für '
    });

    // Attach the datetimepicker handler to input fields with class .datetimepicker
    $('.datetimepicker').datepicker({
        dateFormat: 'dd.mm.yy',
        defaultDate: 0,
        firstDay: 1,
        maxDate: 0
    });

});
