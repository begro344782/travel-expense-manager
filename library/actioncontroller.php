<?php
namespace library;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * ActionController
 *
 * Identifies the requested action from the database, checks access restrictions
 * and returns the specified handler for the action
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */
class ActionController {

    /**
     * @var string  Name of the action to be performed
     */
    private $action;

    /**
     * @var string  Name of the handler class
     */
    private $handler;

    /**
     * @var string  Name of the method in the handler class
     */
    private $method;

    /**
     * @var int     access level required for this action
     */
    private $access;

    /**
     * @var int     login status of the current user
     */
    private $login_status;

    /**
     * __construct
     *
     * Instantiate the ActionController object, identify the action and check
     * for access restrictions
     *
     * @param int $login_status Access level of the currently logged in user
     */
    public function __construct($login_status) {
        $this->login_status = $login_status;

        // Determine which site to load
        try {
            if ($this->getActionIdent(Input::getURLParam('action'))) {
                    $this->access = TRUE;
            } else {
                $this->access = FALSE;
            }
        } catch (RuntimeException $e) {
            // Handle a possible database Error
            ExceptHandler::databaseError($e);
        }
    }

    /**
     * getHandler
     *
     * Check if the access restrictions a satisfied by the current user and
     * return an object of the handler class. The object already knows which
     * method it should call. Throws an \Exception otherwise
     *
     * @return \library\Mixed   Object of the handler class for the requested action
     * @throws \Exception       1015: Action not allowed
     */
    public function getHandler() {
        if ($this->access) {
            // If access is allowed return the handler object
            $handler = __NAMESPACE__ . '\\' . $this->handler;
            return new $handler($this->method);
        } else {
            throw new \Exception("Action not allowed!", 1015);
        }
    }

    /**
     * getActionIdent
     *
     * Get actions form the database and compare the access level to the
     * login status. If the action exists and access is allowed return true,
     * false otherwise.
     *
     * @param string $req_action Requested action
     * @return boolean  True on success, False otherwise
     * @throws \RuntimeException  ERROR_DATABASE_SELECT: Could not get action
     * @see ActionController->properties
     */
    private function getActionIdent($req_action) {
        global $MysqlCon;
        $mysqli = &$MysqlCon;

        $query =  "SELECT act.action, act.handler, act.method, ace.access"
                . "  FROM app_actions AS act"
                . "       JOIN app_access AS ace ON act.access = ace.id"
                . " WHERE act.action = ?"
                . " LIMIT 1";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('s', $req_action);

        $stmt->execute();
        if (!$stmt->store_result()) {
            // If there aren't any action or another error occured, throw error
            throw new RuntimeException("mysqli: Could not get action from database", ERROR_DATABASE_SELECT);
        }
        $stmt->bind_result($action, $handler, $method, $access);
        $stmt->fetch();
        $stmt->close();

        // Make the result available as object properties
        $this->action = $action;
        $this->handler = $handler;
        $this->method = $method;
        $access = intval($access);

        // Set the return value depending on if the user should have access and
        // the object exists
        if ($action !== NULL && $this->login_status >= $access) {
            $ret = TRUE;
        } else {
            $ret = FALSE;
        }
        return $ret;
    }
}
