<?php
namespace library;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * AppActions
 *
 * Provides methods for performing ajax actions related to the application or
 * the user/login system
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */
class AppActions {

    /**
     * @var string Method to be called by the class.
     */
    private $method;

    /**
     * @var Session Session object, because this class might manipulate the session.
     */
    private $session;

    /**
     * __construct
     *
     * Store the method that needs to be called for handling the requested action.
     *
     * @param string $method Method to be called by the class.
     */
    public function __construct($method) {
        $this->method = $method;        // Store requested method for later use.
    }

    /**
     * perform
     *
     * Store the session object in this object and then call the method that was
     * requested.
     *
     * @param  \library\Session $session  Session object of the current session
     */
    public function perform($session) {
        $this->session = $session;      // Store session, just in case
        $this->{$this->method}();       // Call the requested method
    }

    /**
     * login
     *
     * Validate user input and then call session->login to verify if input data
     * is correct. Return success or error message as json encoded string
     *
     * @see  \library\Session->login()
     */
    private function login () {
        // Get and validate the user input
        $email = Input::getEmail('post', 'login_email');
        $password = Input::getPassword('post', 'login_password');

        // Check and perform login (Session object)
        $success = $this->session->login($email, $password);

        // Create success or error message depending on return state of session->login
        if ($success) {
            $return = ['do' => 'redirect', 'target' => 'index.php?site=home'];
        } else {
            $error = 'Login failed: Wrong username or password.';
            $return = ['do' => 'display', 'success' => 'error', 'message' => $error];
        }

        // Send return data as json string
        header('Content-Type: application/json');
        echo json_encode($return);
    }

    /**
     * logout
     *
     * Call Session->logout method and redirect to the login page
     *
     * @see \library\Session->logout()
     */
    private function logout() {
        $this->session->logout();
        header('Location:index.php');
    }

    /**
     * userRegister
     *
     * Get and validate input data for a register request, afterwards call
     * user->register to register the user in the database. Return a json
     * string with a success or error message.
     *
     * @see \library\User->register()
     */
    private function userRegister() {
        // Get and validate user input data
        $firstname  = Input::getName('post', 'firstname');
        $lastname   = Input::getName('post', 'lastname');
        $email      = Input::getEmail('post', 'email');
        $pw         = Input::getPassword('post', 'password');
        $pw_confirm = Input::getPassword('post', 'password_confirm');
        $success = FALSE;

        if ($firstname !== FALSE && $lastname !== FALSE && $email !== FALSE
            && $pw !== FALSE && $pw_confirm !== FALSE) {

            // Check if none of the fields are empty and create a new user object
            $user = new User();

            if ($user->initializeByEmail($email)){
                // Check if the users mail address is already registered
                Application::appendErrorString($errors, 'This address is registered already');
            }

            if ($pw === $pw_confirm) {
                // If the two submitted passwords are equal, hash the password with the salt
                $pw_hash = password_hash($pw, PASSWORD_DEFAULT);
            } else {
                Application::appendErrorString($errors, 'Passwords not equal.');
            }
        } else {
            Application::appendErrorString($errors, 'Please make sure all fields are filled out correctly!');
        }

        if (empty($errors)) {
            // If there weren't any errors yet, try to register the user
            try {
                $success = $user->register($firstname, $lastname, $email, $pw_hash);
            } catch (Exception $e) {
                Application::appendErrorString($errors, ExceptHandler::getMessage($e));
            }
        }

        if ($success) {
            // If everything was successful return a corresponding message and an error otherwise
            $done = 'Successfully registered. You will received a verification email!';
            $return = ['do' => 'display', 'success' => 'success', 'message' => $done];
        } else {
            $return = ['do' => 'display', 'success' => 'error', 'message' => implode('', $errors)];
        }

        // Return the data as a json string
        header('Content-Type: application/json');
        echo json_encode($return);
    }

    /**
     * userPwForgot
     *
     * Get the input email and check if the user exists. If so call user->forgotPW()
     * to send an email and return a message as json string
     *
     * @see  \library\User->forgotPW()
     */
    private function userPwForgot() {
        // Get and validate user input
        $email = Input::getEmail('post', 'email');
        $errors = [];

        $user = new User();
        if ($email !== FALSE) {
            // Check if the user actually exists
            if (!$user->initializeByEmail($email)) {
                Application::appendErrorString($errors,
                    'This address is not registered.');
            }
        } else {
            Application::appendErrorString($errors,
                'Please enter a valid address.');
        }

        if (empty($errors)) {
            // If there weren't any errors yet, call user->forgotPW() to generate
            // token and email
            try {
                $success = $user->forgotPW();
            } catch (Exception $e) {
                Application::appendErrorString($errors, ExceptHandler::getMessage($e));
            }
        } else {
            $success = false;
        }

        if ($success) {
            // If everything was successful return a success message, an error otherwise
            $done = 'You will receive an email with further instructions.';
            $return = ['do' => 'display', 'success' => 'success', 'message' => $done];
        } else {
            $return = ['do' => 'display', 'success' => 'error', 'message' => implode('', $errors)];
        }

        // Return the data as json string
        header('Content-Type: application/json');
        echo json_encode($return);
    }

    /**
     * userPwReset
     *
     * Get a new password and confirm the request using the token created
     * earlier. Then call user->resetPW() to insert the new password into the
     * database.
     *
     * @global \Mysqli $MysqlCon
     * @see  \library\User->resetPW()
     */
    private function userPwReset() {
        global $MysqlCon;
        $mysqli = &$MysqlCon;

        // Get and check input data
        $new_password   = Input::getPassword('post', 'password');
        $password_check = Input::getPassword('post', 'password_confirm');
        $user_id        = Input::getInt('post', 'id');
        $email          = Input::getEmail('post', 'email');
        $random_token   = Input::getString('post', 'token');

        $errors = [];
        $user = new User();
        // Are both passwords set?
        $new_password === FALSE || $password_check === FALSE ?
                Application::appendErrorString($errors,
                        'Password must be at least 6 characters long, contain 2 numbers and some capitals.') : NULL;
        // Is the email set and valid?
        $email === FALSE ?
                Application::appendErrorString($errors, 'Email address invalid.') : NULL;
        // Check if the two submitted passwords are equal
        $new_password !== $password_check ?
                Application::appendErrorString($errors, 'Passwords not equal.') : NULL;
        // Check if the user is already registered
        !$user->initializeByEmail($email) ?
                Application::appendErrorString($errors, 'Email address not registered.') : NULL;

        // Prevent one token to be used multiple times and for longer than 24 hours
        // by fetching token data from database
        $valid_time = (60 * 60 * 24);
        $query = "SELECT id"
               . "  FROM app_actionlog "
               . " WHERE user_id = ? "
               . "       AND random_tok = ? "
               . "       AND tok_used = 0 "
               . "       AND change_date > CURRENT_TIMESTAMP() - FROM_UNIXTIME(?)"
               . " LIMIT 1;";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('isi', $user_id, $random_token, $valid_time);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($tok_id);
        $stmt->fetch();

        // If there is no valid token, don't allow the request
        $stmt->num_rows !== 1 ?
                Application::appendErrorString($errors, 'No matching request found.') : NULL;
        $stmt->close();

        $success = false;
        if (empty($errors)) {
            // If there weren't any errors yet, try to reset the password
            try {
                $success = $user->resetPW($new_password, $tok_id);
            } catch (Exception $e) {
                Application::appendErrorString($errors, ExceptHandler::getMessage($e));
            }
        }

        // Lastly generate a nice message and send it all encoded as json
        $msg = $success ? 'Password reset. You can now use the new password to log in.' :
                    'An unknwon error occured.';
        Application::appendErrorString($errors, $msg);
        if ($success) {
            $return = ['do' => 'display', 'success' => 'success', 'message' => implode('', $errors)];
        } else {
            $return = ['do' => 'display', 'success' => 'error', 'message' => implode('', $errors)];
        }
        header('Content-Type: application/json');
        echo json_encode($return);
    }

    /**
     * changeUser
     *
     * Combines some admin level functions for manipulating users. Gets and checks
     * Input data and the calls appropriate user->? function to perform the necessary
     * actions.
     *
     * @see \library\User->changeAccess()
     * @see \library\User->approve()
     * @see \library\User->delete()
     */
    private function changeUser() {
        // Get and check the input data
        $uid = Input::getInt('post', 'user');
        $action = Input::getString('post', 'btn_action');
        $errors = [];

        // Instantiate user object and check if the user exists
        $user = new User();
        $user->initializeById($uid) ?
                NULL : Application::appendErrorString($errors, "User not found.");

        $success = false;
        if (empty($errors)) {
            // If the user exists call the appropriate method
            switch ($action) {
                case 'access_admin' :
                    // Mark a user as admin
                    $access = 3;
                    $success = $user->changeAccess($access);
                    $success_msg = "User is now an admin.";
                    $data = "Admin";
                    $col = 4;
                    break;
                case 'access_member' :
                    // Mark the user as normal member
                    $access = 2;
                    $success = $user->changeAccess($access);
                    $success_msg = "User is now a member";
                    $data = "Member";
                    $col = 4;
                    break;
                case 'user_approve' :
                    // Approve the user
                    $success = $user->approve();
                    $success_msg = "User approved";
                    $data = "1";
                    $col = 5;
                    break;
                case 'user_delete' :
                    // Delete the user
                    $success = $user->delete();
                    $success_msg = "User deleted";
                    $delete = TRUE;
                    break;
                default :
                    Application::appendErrorString($errors, "Action could not be performed (unknown).");
            }
        }

        // If that was successful generate and return a nice json encoded message
        // or else an error
        if ($success) {
            if (!isset($delete)) {
                $return = ['do' => 'display', 'success' => 'success', 'message' => $success_msg,
                            'update_table' => 'update_cell', 'cell' => ['row' => $uid, 'col' => $col],
                            'data' => $data];
            } else {
                $return = ['do' => 'display', 'success' => 'success', 'message' => $success_msg,
                            'update_table' => 'delete_row', 'update_tr' => $uid];
            }
        } else {
            $return = ['do' => 'display', 'success' => 'error', 'message' => implode('', $errors)];
        }
        header('Content-Type: application/json');
        echo json_encode($return);
    }

    /**
     * userPwUpdate
     *
     * Gets and checks input data for updating the users password. Then calls
     * User->updatePw to do the heavy lifting
     *
     * @see \library\User->updatePw()
     */
    private function userPwUpdate() {
        // Get and verify input data
        $pw = Input::getName('post', 'password');
        $pw_confirm = Input::getName('post', 'password_confirm');
        $pw_old = Input::getName('post', 'password_old');
        $uid = intval($_SESSION['user_id']);
        $errors = [];

        // Instantiate and initialize user object
        $user = new User();
        $user->initializeById($uid);
        $email = $user->getEmail();

        // Try to login again and see that way if the password is correct
        if (!$this->session->login($email, $pw_old)) {
            Application::appendErrorString($errors, 'Old password invalid!');
        }

        // Are both passwords set and valid strings?
        $pw === FALSE || $pw_confirm === FALSE ?
            Application::appendErrorString($errors, 'Password must be at least 6 characters long, contain 2 numbers and some capitals.') : NULL;
        // Check if the two submitted passwords are equal
        $pw !== $pw_confirm ? Application::appendErrorString($errors, 'Passwords not equal.') : NULL;

        $success = false;
        if (empty($errors)) {
            // If everything was successful up to here try to update the password
            try {
                $success = $user->updatePw($pw);
            } catch (Exception $e) {
                Application::appendErrorString($errors, ExceptHandler::getMessage($e));
            }
        }

        // Last generate a nice success or error message to be sent as json object
        if ($success) {
            $done = "Password changed successfully!";
            $return = ['do' => 'display', 'success' => 'success', 'message' => $done];
        } else {
            $return = ['do' => 'display', 'success' => 'error', 'message' => implode('', $errors)];
        }
        header('Content-Type: application/json');
        echo json_encode($return);

    }

    /**
     * userUpdate
     *
     * Get and check all the user information and then call User->update to update
     * the information in the database.
     *
     * @see \library\User->update()
     */
    private function userUpdate() {
        // Get and validate the input data
        $firstname = Input::getName('post', 'firstname');
        $lastname = Input::getName('post', 'lastname');
        $email = Input::getName('post', 'email');
        $uid = intval($_SESSION['user_id']);
        $errors = [];

        // Initialize the user and check if the given email address is not
        // registered yet
        $user = new User();
        $user->initializeById($uid);
        $testuser = new User();
        if ($testuser->initializeByEmail($email)) {
            if ($testuser->getId() !== $uid) {
                Application::appendErrorString($errors, 'Email address already registered!');
            }
        }

        // Check firstname, lastname and email
        $firstname === FALSE ?
                Application::appendErrorString($errors, 'Enter first name.') : NULL;
        $lastname === FALSE ?
                Application::appendErrorString($errors, 'Enter last name.') : NULL;
        $email === FALSE ?
                Application::appendErrorString($errors, 'Enter email address.') : NULL;

        $success = FALSE;
        if (empty($errors)) {
            try {
                // If all checks succeeded try to update the user
                $success = $user->update($firstname, $lastname, $email);
            } catch (Exception $e) {
                Application::appendErrorString($errors, ExceptHandler::getMessage($e));
            }
        }

        // Generate a nice success or error message and send as json object
        if ($success) {
            $done = "Data changed successfully!";
            $return = ['do' => 'display', 'success' => 'success', 'message' => $done];
        } else {
            $return = ['do' => 'display', 'success' => 'error', 'message' => implode('', $errors)];
        }
        header('Content-Type: application/json');
        echo json_encode($return);
    }
}
