<?php
namespace library;
use Config;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * Application
 *
 * Load some other important classes, register the autoloader and provide some
 * further static functionality to be used by other objects.
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */
class Application {

    /**
     * __construct
     *
     * Connect to the database, register the autoloader, register error constants
     * and perform the cron job if necessary
     */
    public function __construct() {
        // Connect to database
        global $MysqlCon;
        $MysqlCon = $this->getDBO();

        // Register autoloader
        spl_autoload_register('spl_autoload');

        // Register custom error constants
        ExceptHandler::registerErrorNums();

        // Perform cron job on every page load
        $this->doCron();
    }

    /**
     * initialize
     *
     * Get the url parameter form actions and depending on that get and return
     * an ActionHandler or a ViewController Object
     * @param int $login_status     Login status of the current user
     * @return \library\ViewController  Object for setting up the view
     * @return \library\ActionHandler   Object for handling actions
     */
    public function initialize($login_status) {
        $action = Input::getURLParam('action');

        if ($action !== FALSE) {
            // If there is an action specified get the action object
            $act = new ActionController($login_status);
            try {
                $obj = $act->getHandler();
            } catch (\Exception $e) {
                // If there was an error display a nice json encoded error message
                $return = ['do' => 'display', 'success' => 'error', 'message' => ExceptHandler::getMessage($e)];
                header('Content-Type: application/json');
                echo json_encode($return);
                die;
            }
        } else {
            // If there was no action parameter we'll load a view
            $obj = new ViewController($login_status);
        }

        // Return the object
        return $obj;
    }

    /**
     * appendErrorString
     *
     * Counts the existing errors and initializes a new array or adds a new line.
     *
     * @todo This function should probably be included in ExceptHandler
     *
     * @static
     * @param Array $error Array of error message strings
     * @param string $newstring The message string to be appended to the array
     * @return Array New array of error message strings
     */
    public static function appendErrorString(&$error, $newstring){
        if (count($error) === 0) {
            // If $error is empty start at id 0 and add a new p element
            $error[0] = $newstring;
        } else {
            // Append new line
            $error[] = '<br />' . $newstring;
        }
        return $error;
    }

    /**
     * sendEmail
     *
     * Send an email to the given email address with supplied subject. Appends
     * a custom site footer. And sets all required headers
     *
     * @static
     * @param string $tos Recipient email address
     * @param string $subject Email subject
     * @param string $msg HTML Email message
     * @throws \Exception ERROR_MAIL_SEND: Email konnte nicht gesendet werden.
     */
    public static function sendEmail($tos, $subject, $msg) {
        $to       = strip_tags($tos);
        $subject  = Config::$subject_prefix . " " . $subject;
        $from     = Config::$from_email;
        $body     = $msg
                 . "<br /><br/>"
                 . "You are receiving this email from " . Config::$sitename
                 . "@" . Config::$page_domain . " because you have an account. "
                 . "If you do not want to receive any more emails, please contact "
                 . Config::$from_email . " by replying to this email ";
        $headers  = "From: " . strip_tags($from) . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        if (!mail($to,$subject,$body,$headers)) {
            throw new \Exception("Email: could not be sent.", ERROR_MAIL_SEND);
        }
    }

    /**
     * prepParamList
     *
     * To prepare Mysql statements with variable numbers of parameters we need
     * an array of referenced copies of the parameter variables. This function
     * creates that array
     *
     * @static
     * @param Array $arr Array of parameters
     * @param string $type Type definition of the parameters in $arr (i,d,s, ...)
     * @return Array Array of referenced copies of $arr
     */
    public static function prepParamList($arr, $type) {
        $types = array(implode('', array_fill(0, count($arr), $type)));
        $refs = [];
        foreach($arr as $key => $value)  {
            $refs[$key] = &$arr[$key];
        }
        $ret = array_merge($types, $refs);

        return $ret;
    }

    /**
     * getDBO
     *
     * Connect to the database using the information from
     * the config file and return a \Mysqli object.
     *
     * @return \Mysqli Database connector object
     */
    private function getDBO() {
        return new \mysqli(Config::$db_host, Config::$db_user, Config::$db_password, Config::$db_name);
    }

    /**
     * doCron
     *
     * Call jobs to be done in every call and make sure they are not performed
     * too often
     */
    private function doCron() {
        global $MysqlCon;
        $mysqli = &$MysqlCon;
        $time_between_crons = 60*60*5; // Only do one job every 5 hours

        // Check if another job has been done in the specified timeframe using
        // the database
        $query =  "SELECT 1 AS exist "
                . "  FROM app_actionlog "
                . " WHERE atype = 'cron'"
                . "       AND UNIX_TIMESTAMP(change_date) > (UNIX_TIMESTAMP() - ?)"
                . " LIMIT 1;";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('i', $time_between_crons);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->affected_rows < 1 && Config::$cron_active === true) {
            // If no other job took place in the given timeframe, execute
            // some functions
            $stmt->close();

            // Remind users with negative balance to pay their dues
            $trip = new Trip();
            $trip->remindNegativeBalance();

            // Insert the finished job into the table
            $update_query = "INSERT INTO app_actionlog (atype) VALUES ('cron');";
            $mysqli->query($update_query);
        } else {
            $stmt->close();
        }
    }
}
