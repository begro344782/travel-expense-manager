<?php
namespace library;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * ExceptHandler
 *
 * Provide functionality to handle exceptions and register
 * custom error constants.
 *
 * @todo Improve error handling everywhere, possibly by improving the functions
 *       available here and making this an extension of the \Exception class
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */
class ExceptHandler {
    /**
     * registerErrorNums
     *
     * Register values for some custom error constants
     *
     * @static
     */
    public static function registerErrorNums () {
        // Database errors
        define('ERROR_DATABASE_SELECT', 1001);
        define('ERROR_DATABASE_INSERT', 1002);
        define('ERROR_DATABASE_UPDATE', 1003);

        // PHP ini and email errors
        define('ERROR_INI_SET', 9001);
        define('ERROR_MAIL_SEND', 9002);

        // User errors
        define('ERROR_USER_INPUT', 2001);
    }

    /**
     * fatalError
     *
     * Echo information about the fatal error
     *
     * @param \Exception $e The Exception object
     */
    public static function fatalError ($e) {
        echo "Fatal Error: " . $e->getMessage();
        echo "Code: " . $e->getCode();
    }

    /**
     * databaseError
     *
     * Echo information about the database error
     *
     * @param \Exception $e The Exception object
     */
    public static function databaseError ($e) {
        echo "Database Error: " . $e->getMessage();
        echo "Code: " . $e->getCode();
    }

    /**
     * getMessage
     *
     * Return the error message.
     *
     * @param \Exception $e The Exception object
     * @return string Error message
     */
    public static function getMessage ($e) {
        return $e->getMessage();
    }
}
