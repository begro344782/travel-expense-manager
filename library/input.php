<?php
namespace library;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * Input
 *
 * Get and verify data from input requests
 *
 * @static
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */
class Input {

    /**
     * getURLParam
     *
     * Check if the requested parameter exists and get it from the url string.
     *
     * @static
     * @param string $input_name Name of the requested parameter
     * @return string String of the Parameter value, FALSE on failure
     */
    public static function getURLParam($input_name) {
        if (filter_has_var(INPUT_GET, $input_name)) {
            // If the parameter exists filter it and return it.
            $options = array('flags' => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH|FILTER_FLAG_STRIP_BACKTICK);
            $input = filter_input(INPUT_GET, $input_name, FILTER_SANITIZE_STRING, $options);
            return $input;
        } else {
            // If the parameter does not exist return false.
            return FALSE;
        }
    }

    /**
     * getString
     *
     * Get a string from the request
     *
     * @static
     * @param  string $input_type Superglobal array to search in
     * @param  string $input_name Name of the variable to look for
     * @return string Filtered value of the variable or FALSE on failure
     */
    public static function getString($input_type, $input_name) {
        // Choose constant for the requested input type
        $type_const = self::chooseInputType($input_type);
        if (filter_has_var($type_const, $input_name)) {
            // Check if variable exists, filter and return it
            $input = filter_input($type_const, $input_name, FILTER_SANITIZE_STRING);
            return $input;
        } else {
            // Return false otherwise
            return FALSE;
        }
    }

    /**
     * getInt
     *
     * Get an integer from the request
     *
     * @static
     * @param  string $input_type Superglobal array to search in
     * @param  string $input_name Name of the variable to look for
     * @return int Filtered value of the variable or FALSE on failure
     */
    public static function getInt($input_type, $input_name) {
        // Choose constant for the requested input type
        $type_const = self::chooseInputType($input_type);
        if (filter_has_var($type_const, $input_name)) {
            // Check if the variable exists, filter, validate and return it
            $input = filter_input($type_const, $input_name, FILTER_SANITIZE_NUMBER_INT);
            $input = filter_var($input, FILTER_VALIDATE_INT);
            return $input;
        } else {
            // Return false otherwise
            return FALSE;
        }
    }

    /**
     * getFloat
     *
     * Get a floatingpoint valued from the request
     *
     * @static
     * @param  string $input_type Superglobal array to search in
     * @param  string $input_name Name of the variable to look for
     * @return float Filtered value of the variable or FALSE on failure
     */
    public static function getFloat($input_type, $input_name) {
        // Choose constant for the requested input type
        $type_const = self::chooseInputType($input_type);
        if (filter_has_var($type_const, $input_name)) {
            // If the variable exists, filter it as string
            $input = filter_input($type_const, $input_name, FILTER_SANITIZE_STRING);
            // Try to catch at least some formatting errors especially concerning
            // locale errors
            if (strpos($input, ',') !== FALSE && strpos($input, '.') === FALSE) {
                $input = str_replace(',', '.', $input);

            } elseif (strpos($input, ',') !== FALSE && strpos($input, '.') !== FALSE) {
                // If number in German format is found, change format
                if (strpos($input, ',') > strpos($input, '.')) {
                    $input = str_replace('.', '', $input);
                    $input = str_replace(',', '.', $input);
                }
            }

            // Now sanitize the float and validate it
            $input = filter_var($input, FILTER_SANITIZE_NUMBER_FLOAT,
                    FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND);
            $input = filter_var($input, FILTER_VALIDATE_FLOAT);
            return $input;
        } else {
            return FALSE;
        }
    }

    /**
     * getName
     *
     * Get a name from the request
     *
     * @todo Improve the checks here, maybe using ctype_alpha function.
     *
     * @static
     * @param  string $input_type Superglobal array to search in
     * @param  string $input_name Name of the variable to look for
     * @return string Filtered value of the variable or FALSE on failure
     */
    public static function getName($input_type, $input_name) {
        $name = self::getString($input_type, $input_name);

        // Name may only contain letters and whitespace
        $re = '/^\w+$/';
        if (preg_match($re, $name) === NULL) {
            // If no match is found, return FALSE
            return FALSE;
        } else {
            return $name;
        }
    }

    /**
     * getEmail
     *
     * Get an email address from the request
     *
     * @static
     * @param  string $input_type Superglobal array to search in
     * @param  string $input_name Name of the variable to look for
     * @return string Filtered value of the variable or FALSE on failure
     */
    public static function getEmail($input_type, $input_name) {
        $email = self::getString($input_type, $input_name);

        // Check if email is valid using filter_var with FILTER_VALIDATE_EMAIL
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        if ($email === FALSE) {
            // Don't return anything if no match is found
            return FALSE;
        }
        //Otherwise return email address
        return $email;
    }

    /**
     * getPassword
     *
     * Get the password as string and the check its integrity against a regex.
     *
     * @todo The check does not currently work because passwords are submitted as
     *       SHA512 Hash using javascript. Therefore they always pass this check...
     *
     * @static
     * @param  string $input_type Superglobal array to search in
     * @param  string $input_name Name of the variable to look for
     * @return string Filtered value of the variable or FALSE on failure
     */
    public static function getPassword($input_type, $input_name) {
        $password = self::getString($input_type, $input_name);

        // Password must contain one digit, one lowercase and one uppercase letter
        // and be at least 6 characters in length
        $re = '/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/';
        if (preg_match($re, $password) === NULL) {
            // If no match was found, return nothing
            return FALSE;
        }

        return $password;
    }

    /**
     * chooseInputType
     *
     * Switch between different input type constants
     *
     * @static
     * @param  string $type Input type
     * @return int          Input type constant
     */
    protected static function chooseInputType($type) {
        $type = strtolower($type);
        switch ($type) {
            case 'get':
                $type_const = INPUT_GET;
                break;
            case 'post':
                $type_const = INPUT_POST;
                break;
            case 'server':
                $type_const = INPUT_SERVER;
                break;
            default:
                $type_const = INPUT_GET;
                break;
        }
        return $type_const;
    }
}
