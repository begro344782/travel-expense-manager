<?php
namespace library;
use Config;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * Session
 *
 * Authenticate users, create new sessions, logout and destroy sessions. The
 * Authentication functions provided by \library\UserAuth should execute
 * approximately in equal time.
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */
class Session extends UserAuth {

    /**
     * @var string  Session check string
     */
    protected $check_string;

    /**
     * __construct
     *
     * Start a new session, generate and save the session check string
     */
    public function __construct() {
        $this->start();
        $this->check_string = $this->createSessionCheckString();
    }

    /**
     * start
     *
     * Create a session and set the session cookie with the configured parameters
     */
    public function start() {
        // Set session name
        $session_name = Config::$sitename;

        // Force sessions to only use cookies.
        try {
            if (!ini_set('session.use_only_cookies', 1) ||
                !ini_set('session.gc_maxlifetime', 1800) ||
                !ini_set('session.cache_limiter', 'nocache')) {
                throw new Exception("ini_set: Setting session parameters unsuccessful", 0001);
            }
        } catch (Exception $e) {
            ExceptHandler::fatalError($e);
        }

        // Gets current cookies params.
        session_set_cookie_params(0, "/", Config::$page_domain, Config::$secure, true);

        // Sets the session name to the one set above and starts the session
        session_name($session_name);
        session_start();
        session_regenerate_id(true);
    }

    /**
     * login
     *
     * Checks login variables and verifies login information. Then redirects
     * as necessary.
     *
     * @param string $login_email E-Mail of the user that is trying to login
     * @param string $login_password Password that is given for the login attempt
     * @return boolean True if login was successful, false otherwise
     */
    public function login($login_email, $login_password){
        $success = FALSE;

        // Check if email and password were submitted and are not empty
        if (is_string($login_email) && is_string($login_password)) {
            if ($this->loginCheck($login_email, $login_password)) {
                // If login credentials check out create a new session
                $this->createLoginSession($this->getUserId(),
                        $this->firstname, $this->lastname, $this->email, $this->access);
                $success = TRUE;
            }
        }
        return $success;
    }

    /**
     * logout
     *
     * Logs out a user and destroys the session.
     *
     * @return boolean True if logout was successfull
     */
    public function logout() {
        // Unset all session values
        session_unset();

        // Reset the cookie.
        setcookie(session_name(), '',
                  time() - 42000, '/',
                  Config::$page_domain,
                  Config::$secure,
                  TRUE);

        // Destroy session
        session_destroy();

        return TRUE;
    }

    /**
     * checkLoginStatus
     *
     * Checks the users authentication status and returns the current status
     * as an integer
     *
     * @todo Allow creation of customized access levels using database
     *
     * @return int The users access level
     */
    public function checkLoginStatus() {
        // Check if the session variables are set
        if (isset($_SESSION['user_id'])) {
            $user_input_pass = $_SESSION['login_string'];
            // Load the check string
            $login_check_string = $this->check_string;

            // Check if both strings match
            if ($login_check_string == $user_input_pass) {
                $login_status = $_SESSION['access'];
            }
        }

        // If that didn't work the user isn't logged in
        if (!isset($login_status)) {
            $login_status = 0;
        }

        return $login_status;
    }

    /**
     * createLoginSession
     *
     * Create a new session for a freshly logged in user and save important
     * values into session variables.
     *
     * @param int    $user_id  ID of the user to create the session for
     * @param string $firstname Firstname of the authenticated user
     * @param string $lastname Lastname of the authenticated user
     * @param string $email E-Mail of the authenticated user
     * @param int $access Access level of user, Default: 0
     */
    protected function createLoginSession($user_id, $firstname, $lastname, $email, $access = 0) {
        // XSS Protection as we might print these values
        $user_id = preg_replace("/[^0-9]+/", "", $user_id);
        $firstname = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $firstname);
        $lastname = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $lastname);

        // Create session variables
        $_SESSION['user_id']      = $user_id;
        $_SESSION['firstname']    = $firstname;
        $_SESSION['lastname']     = $lastname;
        $_SESSION['email']        = $email;
        $_SESSION['access']       = $access;
        $_SESSION['login_string'] = $this->check_string;
    }

    /**
     * createSessionCheckString
     *
     * Creates a hashed string for identifying a users session by using their
     * browser and ip information
     *
     * @return string hashed login check string
     */
    protected function createSessionCheckString() {
        // Save the users user agent string
        $user_browser = Input::getString('server', 'HTTP_USER_AGENT');
        $user_ip = Input::getString('server', 'REMOTE_ADDR');
        $user_proxy = Input::getString('server', 'HTTP_X_FORWARDED_FOR');

        return hash('sha512', $user_ip . $user_proxy . $user_browser);
    }
}
