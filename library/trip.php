<?php
namespace library;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * Trip
 *
 * Manage trips, get trips and balance and store or delete trips. Also manages
 * everything related to trips such as cars and fuels
 *
 * @todo Improve error handling for database queries to prevent some data
 *       being written while other is not
 * @todo Separate some functions into other classes for example cars, fuels
 *       and user related stuff
 * @todo Rewrite SQL queries to be more flexible. Possibly generate more of them
 *       on the fly to select only the necessary data
 * @todo Maybe another database view would be helpful that collects all trip info
 *       in one place
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */
class Trip {

    /**
     * getTripsByUser
     *
     * Get all trips for a specified user, separating between rides and drives
     * and arrange them into an array
     *
     * @todo Combine as much as possible with other listing functions
     *
     * @global \Mysqli $MysqlCon Mysql connector object
     * @param int $user_id User id of the user to get the trips for
     * @return Array trips separated sorted by rides and drives
     */
    public function getTripsByUser($user_id) {
        global $MysqlCon;
        $mysqli = &$MysqlCon;

        // Get the trips from database. Concatenate the passenger names, excluding
        // the drivers name, the drivers name is selected into a separate field.
        // Also formats the date nicely and rounds the price for display.
        $query =    "  SELECT tri.id AS trip_id, "
                  . "         cos.driver AS driver, "
                  . "         GROUP_CONCAT( "
                  . "                      CASE WHEN pas.driver = 1 "
                  . "              	      THEN NULL "
                  . "                           ELSE CONCAT(mem.firstname, ' ', mem.lastname) "
                  . "            		 END "
                  . "                      ORDER BY mem.lastname ASC  "
                  . "                      SEPARATOR ', ') AS pas_names, "
                  . "         MAX(CASE WHEN pas.driver = 1 "
                  . "                  THEN CONCAT(mem.firstname, ' ', mem.lastname) "
                  . "                  ELSE NULL "
                  . "             END) AS driver_name, "
                  . "         ROUND(cos.price, 2) AS cost, "
                  . "         DATE_FORMAT(tri.change_date, '%d.%m.%Y') AS datum "
                  . "    FROM trip_passengercost cos  "
                  . "         JOIN trip_trip tri ON cos.trip = tri.id "
                  . "         JOIN trip_passengercost pas ON cos.trip = pas.trip "
                  . "         JOIN user_user mem ON  pas.user_id = mem.id "
                  . "   WHERE cos.user_id = " . $user_id . " "
                  . "GROUP BY tri.id "
                  . "ORDER BY tri.change_date;";
        $result = $mysqli->query ($query);

        $trips = [];
        if ($result->num_rows >= 1) {
            // If some rows were found, insert them into the assoc array
            while ($row = $result->fetch_assoc()) {
                // Fetch all rows as associative arrays and separate them by drives
                // and rides.
                if ($row['driver'] == 0) {
                    $trips['rides'][] = $row;
                } else {
                    $trips['drives'][] = $row;
                }
            }
            $ret = $trips;
        } else {
            $ret = FALSE;
        }
        $result->close();
        return $ret;
    }

    /**
     * getBalanceList
     *
     * Get a list of users and usernames including more information about
     * their access levels and cost balance.
     *
     * @todo This should be combined with the other listing functions in this class
     *       and some of it should be separated into User because it doesn't have
     *       anything to do with trips.
     * @todo The function needs some more error handling for sql queries
     *
     * @global \Mysqli $MysqlCon Mysql connector object
     * @param Array $ids List of user ids to select
     * @return Array Associative array with all user and trip info
     */
    public function getBalanceList($ids) {
        global $MysqlCon;
        $mysqli = &$MysqlCon;

        // Prepare the query (the $instr must be generated like this because the
        // id list is of variable length)
        $instr = implode(',', $ids);
        $query =  "  SELECT mem.id, CONCAT(mem.firstname, ' ', lastname) AS mname, "
                . "         mem.email, mem.access, acc.title AS access_name, mem.approved, "
                . "         mem.deleted, ROUND(bal.cost, 2) AS cost, "
                . "         ROUND(bal.deposit, 2) AS deposit, ROUND(bal.total, 2) AS balance"
                . "    FROM trip_userbalance bal  "
                . "         JOIN user_user mem ON bal.user_id = mem.id "
                . "         JOIN app_access acc ON mem.access = acc.id "
                . "   WHERE mem.id IN (" . $instr . ") "
                . "         AND mem.deleted = 0 "
                . "ORDER BY mem.lastname;";
        $result = $mysqli->query($query);
        $list = [];
        if ($result->num_rows >= 1) {
            // If some rows were found, insert them into the assoc array
            while ($row = $result->fetch_assoc()) {
                $list[] = $row;
            }
            $ret = $list;
        } else {
            $ret = FALSE;
        }
        $result->close();
        return $ret;
    }

    /**
     * getDepositsBalance
     *
     * Selects all deposits and the balance as well as some additional user information
     * for the given user id.
     *
     * @todo Combine with getBalanceList because they do quite similar things
     *
     * @global \Mysqli $MysqlCon Mysql connector object
     * @param int $uid The user is
     * @return Array Associative array of deposits, including user information
     */
    public function getDepositsBalance($uid) {
        global $MysqlCon;
        $mysqli = &$MysqlCon;

        // Prepare the query to get user info and a list of deposits
        $query =    "  SELECT mem.id AS user_id, mem.firstname, mem.lastname, mem.email, bal.cost AS tot_cost, "
                  . "         bal.deposit AS tot_deposit, bal.total, dep.deposit, dep.change_date "
                  . "    FROM user_user mem "
                  . "         LEFT JOIN trip_deposit dep ON mem.id = dep.user_id "
                  . "         LEFT JOIN trip_userbalance bal ON mem.id = bal.user_id "
                  . "   WHERE mem.id = " . $uid . ";";
        $result = $mysqli->query($query);
        $list = [];
        if ($result->num_rows >= 1) {
            // If some rows were found, insert them into the assoc array
            while ($row = $result->fetch_assoc()) {
                $list[] = $row;
            }
            $ret = $list;
        } else {
            $ret = FALSE;
        }
        $result->close();
        return $ret;
    }

    /**
     * getUserCar
     *
     * Retrieve the car for a specified user from the database, making sure
     * to select the latest car and the latest fuel prices.
     *
     * @todo Combine with getDepositsBalance because that also gets the car
     *       and fuel prices.
     *
     * @global \Mysqli $MysqlCon Mysql connector object
     * @param int $uid  User id of the requesting user
     * @return Array Associative array of car information
     */
    public function getUserCar($uid) {
        global $MysqlCon;
        $mysqli = &$MysqlCon;
        $car = [];

        // Prepare query to select the car and fuel prices from the database.
        // Where clause makes sure to select the most current info
        $query =    "  SELECT car.id, car.seats, car.consumpt, fuen.id, "
                  . "         fuen.ftype, fuep.price "
                  . "    FROM trip_usercar car "
                  . "         JOIN trip_fuel fuen ON car.fuel = fuen.id "
                  . "         JOIN trip_fuel_price fuep ON fuen.id = fuep.fuel "
                  . "   WHERE car.cowner = ? "
                  . "         AND car.deleted <> 1 "
                  . "         AND car.id = (  SELECT car2.id "
                  . "                           FROM trip_usercar car2 "
                  . "                          WHERE car2.cowner = car.cowner "
                  . "                       ORDER BY car2.id DESC "
                  . "                          LIMIT 1) "
                  . "         AND fuep.id = (  SELECT fuep2.id "
                  . "                            FROM trip_fuel_price fuep2 "
                  . "                           WHERE fuep2.fuel = fuep.fuel "
                  . "                        ORDER BY fuep2.id DESC "
                  . "                           LIMIT 1)"
                  . "   LIMIT 1;";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('i', $uid);
        $stmt->execute();
        $stmt->store_result();

        if ($stmt->num_rows !== 1) {
            $ret = FALSE;
        } else {
            // If a car was found bind the info into a nicely formatted array
            $stmt->bind_result($id, $seats, $consumpt, $fuelid, $fueltype, $fuelprice);
            $stmt->fetch();
            $ret['id'] = intval($id);
            $ret['seats'] = intval($seats);
            $ret['consumpt'] = floatval($consumpt);
            $ret['fuelid'] = intval($fuelid);
            $ret['fueltype'] = $fueltype;
            $ret['fuelprice'] = floatval($fuelprice);
        }
        $stmt->close();

        return $ret;
    }

    /**
     * getFuelList
     *
     * Get a list of fuels from the database.
     *
     * @global \Mysqli $MysqlCon Mysql connector object
     * @return array Associative array of fuel information
     */
    public function getFuelList() {
        global $MysqlCon;
        $mysqli = &$MysqlCon;
        $fuels = [];

        // Prepare the database query
        $query =    "  SELECT fue.id, fue.ftype, pri.price "
                  . "    FROM trip_fuel fue "
                  . "         JOIN trip_fuel_price pri ON fue.id = pri.fuel "
                  . "   WHERE pri.id = (  SELECT pri2.id "
                  . "                       FROM trip_fuel_price pri2 "
                  . "                      WHERE pri.fuel = pri2.fuel "
                  . "                   ORDER BY pri2.id DESC "
                  . "                   LIMIT 1)"
                  . "ORDER BY fue.ftype; ";
        $stmt = $mysqli->prepare($query);
        $stmt->execute();
        $stmt->store_result();

        if ($stmt->num_rows >= 1) {
            $stmt->bind_result($id, $ftype, $price);
            // If some rows were found, insert them into the assoc array
            while ($stmt->fetch()) {
                $row['id'] = $id;
                $row['ftype'] = $ftype;
                $row['price'] = $price;
                $fuels[] = $row;
            }
            $ret = $fuels;
        } else {
            $ret = FALSE;
        }
        $stmt->close();
        return $ret;
    }

    /**
     * storeTrip
     *
     * Do some error checking and then insert a new trip into the database.
     * Afterwards insert all passengers into the db.
     *
     * @todo Combine the input error checking with other functions
     *
     * @global \Mysqli $MysqlCon Mysql connector object
     * @param string $date Date of the trip
     * @param Array $passengers Array of all passenger user ids
     * @param int $driver User id of the driver
     * @param float $distance Driven distance in kilometers
     * @param boolean $driver_pays Flag if the driver also pays
     * @param boolean $calc_price Flag if the price should be calculated or taken from
     *                            user input
     * @param float $price_in Input value for the price, only needs to be given
     *                        when $calc_price is FALSE
     * @return boolean True on success
     * @throws \Exception ERROR_USER_INPUT: Different errors on user input
     * @throws \Exception ERROR_DATABASE_INSERT: Errors when inserting rows into the db.
     */
    public function storeTrip($date, $passengers, $driver, $distance,
                              $driver_pays = TRUE, $calc_price = TRUE, $price_in = NULL){
        $car_array = $this->getUserCar(intval($driver));

        global $MysqlCon;
        $mysqli = &$MysqlCon;

        // Do some more validity checking of the input data
        if ($car_array === FALSE && $calc_price === TRUE) {
            // If there is no car and the price should be calculated, that doesn't
            // work.
            throw new \Exception("Dem Fahrer ist kein Auto zugeordnet.", ERROR_USER_INPUT);
        } elseif ($car_array === FALSE) {
            // If the driver has no car, set some generic defaults because they
            // are needed for later checks
            $car = NULL;
            $seats = 9;
        } else {
            // If the price should be calculate and the user has a car get db info
            $car = $car_array['id'];
            $seats = $car_array['seats'];
            $consumpt = $car_array['consumpt'];
            $fuelprice = $car_array['fuelprice'];
        }

        // There shouldn't be more passengers than seats in the car (minus the driver)
        if (count($passengers) > $seats - 1 && $calc_price === TRUE) {
            throw new \Exception("Es wurden mehr Passagiere angegeben, als Plätze im Auto vorhanden sind", ERROR_USER_INPUT);
        }

        // If we're supposed to take the user supplied price, there must be one
        if ($calc_price === FALSE && $price_in === NULL) {
            throw new \Exception("Kein Preis angegeben.", ERROR_USER_INPUT);
        } else {
            // calculate the price or set it from input
            $price = $calc_price ? $consumpt / 100 * $fuelprice * $distance : $price_in;
        }

        // Insert data into trips table
        $query1 =    " INSERT INTO trip_trip (car, distance, driver_pays, fixed_price, change_date) "
                  . "  SELECT ? AS car, "
                  . "         ? AS distance, "
                  . "         ? AS driver_pays, "
                  . "         ? AS fixed_price, "
                  . "         STR_TO_DATE(?, '%d.%m.%Y %h')  AS change_date;";
        $stmt1 = $mysqli->prepare($query1);
        $driver_pays = intval($driver_pays);
        $stmt1->bind_param('idids', $car, $distance, $driver_pays, $price, $date);
        $stmt1->execute();
        if ($stmt1->errno != 0) {
            throw new \Exception('Fahrt konnte nicht eingefügt werden.', ERROR_DATABASE_INSERT);
        }
        $trip_id = $stmt1->insert_id;
        $stmt1->close();

        // Insert data into passengers table (one by one)
        $query2 =    "  INSERT INTO trip_passenger (trip, user_id, driver) "
                  . "  VALUES (?, ?, ?);";
        $stmt2 = $mysqli->prepare($query2);
        $drives = 0;
        foreach ($passengers as $passenger) {
            // First insert all passengers
            $stmt2->bind_param('iii', $trip_id, $passenger, $drives);
            $stmt2->execute();
            if ($stmt2->errno != 0) {
                throw new \Exception('Fahrt konnte nicht eingefügt werden.', ERROR_DATABASE_INSERT);
            }
        }
        $drives = 1;

        // Then insert the driver as well
        $stmt2->bind_param('iii', $trip_id, $driver, $drives);
        $stmt2->execute();
        if ($stmt2->errno != 0) {
            throw new \Exception('Fahrt konnte nicht eingefügt werden.', ERROR_DATABASE_INSERT);
        }
        $stmt2->close();
        return true;
    }

    /**
     * deleteTrip
     *
     * Delets a trip with a given id from the database and also makes sure the
     * passengers are deleted first.
     *
     * @global \Mysqli $MysqlCon Mysql connector object
     * @param int $trip_id Id of the trip to be deleted
     * @return boolean True on success
     * @throws \Exception ERROR_DATABASE_UPDATE: Couldn't delete trip
     */
    public function deleteTrip($trip_id) {
        global $MysqlCon;
        $mysqli = &$MysqlCon;

        // Prepare queries to delete passengers and the trip
        $query1 = "DELETE FROM trip_passenger WHERE trip = ?;";
        $query2 = "DELETE FROM trip_trip WHERE id = ?;";
        $stmt1 = $mysqli->prepare($query1);
        $stmt2 = $mysqli->prepare($query2);
        $stmt1->bind_param('i', $trip_id);
        $stmt2->bind_param('i', $trip_id);
        $stmt1->execute();
        $stmt2->execute();

        // Throw an exception with a fitting message depending on the situation
        if ($stmt1->errno != 0 || $stmt2->errno != 0) {
            throw new \Exception('Fahrt konnte nicht gelöscht werden.', ERROR_DATABASE_UPDATE);
        }
        if ($stmt1->affected_rows === 0 || $stmt2->affected_rows === 0) {
            throw new \Exception('Fahrt existiert nicht.', ERROR_DATABASE_UPDATE);
        }

        // If everything went fine close the statements and return true
        $stmt1->close();
        $stmt2->close();
        return true;
    }

    /**
     * addDeposit
     *
     * Add a deposit with a given amount for a given user.
     *
     * @global \Mysqli $MysqlCon Mysql connector object
     * @param int $user User id to add the deposit for
     * @param float $amount Amount to be added
     * @return boolean True on success
     * @throws \Exception ERROR_DATABASE_INSERT Could not save deposit
     */
    public function addDeposit($user, $amount) {
        global $MysqlCon;
        $mysqli = &$MysqlCon;
        // Prepare the insert statement into the deposit table
        $query = "INSERT INTO trip_deposit (user_id, deposit)"
                . "VALUES (?, ?)";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('id', $user, $amount);
        $stmt->execute();

        // If there was an error, throw an appropriate message
        if ($stmt->errno != 0) {
            throw new \Exception('Guthaben konnte nicht gespeichert werden.', ERROR_DATABASE_INSERT);
        }
        if ($stmt->affected_rows === 0) {
            throw new \Exception('Benutzer existiert nicht.', ERROR_DATABASE_INSERT);
        }

        // If all went well close the statement and return true
        $stmt->close();
        return true;
    }

    /**
     * updateCar
     *
     * Inserts some new information about a car into the database. We don't need
     * to worry here whether the car exists because we'll always just add a new
     * line to create a history.
     *
     * @global \Mysqli $MysqlCon Mysql connector object
     * @param int $fuel Id of the fuel that the car runs on
     * @param int $uid Id of the user, the car is associated to
     * @param float $consumpt Consumption of fuel in l/100km
     * @param int $seats Number of seats in the car
     * @param int $delete Is the car deleted or not
     * @return boolean True on success
     * @throws \Exception ERROR_DATABASE_INSERT: Could not insert car
     */
    public function updateCar($fuel, $uid, $consumpt, $seats, $delete) {
        global $MysqlCon;
        $mysqli = &$MysqlCon;
        // Prepare the inserting statement
        $query = "INSERT INTO trip_usercar (fuel, cowner, consumpt, seats, deleted)"
                . "VALUES (?, ?, ?, ?, ?)";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('iidii', $fuel, $uid, $consumpt, $seats, $delete);
        $stmt->execute();

        // If there was an error, throw an exception
        if ($stmt->errno != 0) {
            throw new \Exception('Auto konnte nicht gespeichert werden.', ERROR_DATABASE_INSERT);
        }

        // If all went well, close the statement and return true
        $stmt->close();
        return true;
    }

    /**
     * updateFuel
     *
     * Inserts a new, updated fuel price into the database
     *
     * @global \Mysqli $MysqlCon Mysql connector object
     * @param int $fuelid Id of the fuel that is to be updated
     * @param float $price New fuel price
     * @return boolean True on success
     * @throws \Exception ERROR_DATABASE_INSERT: Could not insert car
     */
    public function updateFuel($fuelid, $price) {
        global $MysqlCon;
        $mysqli = &$MysqlCon;
        // Prepare the inserting statement
        $query = "INSERT INTO trip_fuel_price (fuel, price)"
                . "VALUES (?, ?)";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('id', $fuelid, $price);
        $stmt->execute();

        // If there was an error, throw an exception
        if ($stmt->errno != 0) {
            throw new \Exception('Treibstoff konnte nicht gespeichert werden.', ERROR_DATABASE_INSERT);
        }

        // If all went well, close the statement and return true
        $stmt->close();
        return true;
    }

    /**
     * remindNegativeBalance
     *
     * Get a list of users from the table that have a negative balance and weren't
     * reminded for some time. Then send them a reminder email and note that in
     * the database.
     *
     * @todo implement logging for cronjobs
     *
     * @global \Mysqli $MysqlCon Mysql connector object
     */
    public function remindNegativeBalance() {
        // Get users with negative balance from database
        // Only send a few emails at once
        global $MysqlCon;
        $mysqli = &$MysqlCon;
        $query =  "  SELECT mem.id, mem.firstname, mem.email, bal.total, las.change_date "
                . "    FROM trip_userbalance bal "
                . "         JOIN user_user mem ON bal.user_id = mem.id "
                . "         LEFT JOIN (SELECT log.user_id, log.change_date "
                . "                      FROM app_actionlog log "
                . "                     WHERE log.atype = 'remind_pay') las "
                . "             ON bal.user_id = las.user_id "
                . "   WHERE (bal.total + 0.5) < 0 "
                . "         AND mem.deleted = 0 "
                . "         AND (las.change_date IS NULL "
                . "              OR las.change_date < (UNIX_TIMESTAMP(CURRENT_TIMESTAMP()) - 60*60*24*7*2))"
                . "   LIMIT 15;";

        $stmt = $mysqli->prepare($query);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($user_id, $firstname, $user_email, $balance, $change_date);

        // Prepare a query for inserting the last reminded user
        $query2 =   "INSERT INTO app_actionlog (atype, user_id) "
                  . "VALUES ('remind_pay', ?);";
        $ins_stmt = $mysqli->prepare($query2);

        while($stmt->fetch()){
            // Create and send a nice reminder email to pay new deposit
            $show_balance = number_format(floatval($balance), 2, ',', '.');
            $tomail = $user_email;
            $subject="TravelExpenses: Balance low";
            $body = '<p>Hello ' . $firstname . ',<br /><br />'
                    . 'This is the travel expense manager, your balance is currently negative. '
                    . 'Please make sure to make a deposit as soon as possible. '
                    . '<br /><br />Your current balance: ' . $show_balance . '&euro;. '
                    . '<br /><br />Thank you, the Travel Expense Manager.</p>';

            try {
                // Send the email and insert last reminded user in db.
                Application::sendEmail($tomail, $subject, $body);
                $ins_stmt->bind_param('i', $user_id);
                $ins_stmt->execute();
            } catch (\Exception $e) {
                // If there was an exception it is just discarded because this
                // Function is supposed to run in the background
            }
        }
        $stmt->close();
        $ins_stmt -> close();
    }
}
