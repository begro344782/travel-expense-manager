<?php
namespace library;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * TripActions
 *
 * Action Handler for trip related action, provides methods for checking user
 * input and then calls functions from Trip to add or change trips, cars and fuels
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */
class TripActions {

    /**
     * @var string Method to be called by the class.
     */
    private $method;

    /**
     * @var Session Session object because this class might manipulate the session.
     */
    private $session;

    /**
     * __construct
     *
     * Store the method that needs to be called for handling the requested action.
     *
     * @param string $method Method to be called by the class.
     */
    public function __construct($method) {
        $this->method = $method;
    }

    /**
     * perform
     *
     * Store the session object in this object and then call the method that was
     * requested.
     *
     * @param  \library\Session $session  Session object of the current session
     */
    public function perform($session) {
        $this->session = $session;
        $this->{$this->method}();
    }

    /**
     * addTrip
     *
     * Get, validate and check input data for adding a trip, considering the
     * differences between adding a trip as a normal user and as an admin.
     *
     * @see \library\Trip->storeTrip()
     */
    private function addTrip() {
        $errors = [];
        // Get and validate input data
        $passengers = Input::getString('post', 'passengers_chosen'); // comma separated list of passenger ids
        $datum = Input::getString('post', 'date');
        $distance = 31.4;
        $success = FALSE;

        if ($this->session->checkLoginStatus() >= 3 && Input::getString('post', 'admin') !== FALSE) {
            // If the user is allowed to make additional requests get those inputs as well
            $driver = Input::getInt('post', 'driver_chosen');
            $driver_pays =
                    Input::getString('post', 'driver_pays') === 'on' ? TRUE : FALSE;
            $calc_price =
                    Input::getString('post', 'calc_price') === 'on' ? TRUE : FALSE;
        } else {
            // If the user isn't allowed to manually set these, choose some defaults
            $driver = $_SESSION['user_id'];
            $driver_pays = TRUE;
            $calc_price = TRUE;
        }

        if (!$calc_price) {
            // If the price is given by the user, get that and make sure its valid
            $price = Input::getFloat('post', 'price');
            if ($price === FALSE || $price == 0) {
                Application::appendErrorString($errors, 'Please enter the price!');
            }
        } else {
            // If the price is calculated later, just set it to NULL for now
            $price = NULL;
        }

        if (!$passengers || !$datum || !$driver) {
            // Make sure all relevant data has been given
            Application::appendErrorString($errors, 'Please fill out all fields!');
        } else {
            // The passengers must be exploded into an array so some additional
            // error checking can be done
            $pas_array = explode(",", $passengers);
            foreach ($pas_array as $key => $value) {
                $pas_array[$key] = intval(trim($value));
            }

            if (count($pas_array) <= 0) {
                Application::appendErrorString($errors, 'No passengers entered.');
            } else {
                // Go through an array of users to make sure all the passengers acutally
                // exist
                $user = new User();
                $users = $user->getUsersList();

                foreach ($users as $user) {
                    $user_ids[] = $user['id'];
                }
                foreach($pas_array as $passenger) {
                    // Check if each submitted passenger is a valid user
                    if (!in_array($passenger, $user_ids)) {
                        Application::appendErrorString($errors, 'Passengers not found.');
                    }
                }
            }
        }

        if (empty($errors)) {
            // If there weren't any errors upt to here attempt to store the trip
            $trip = new Trip();
            try{
                $success = $trip->storeTrip($datum, $pas_array, $driver, $distance, $driver_pays, $calc_price, $price);
            } catch (\Exception $e) {
                Application::appendErrorString($errors, ExceptHandler::getMessage($e));
            }
        }

        if ($success) {
            // When everything was successful return a nice success or error message
            $done = 'Trip added succcessfully!';
            $return = ['do' => 'display', 'success' => 'success', 'message' => $done];
        } else {
            $return = ['do' => 'display', 'success' => 'error', 'message' => implode('', $errors)];
        }
        // Encode return data as json string
        header('Content-Type: application/json');
        echo json_encode($return);
    }

    /**
     * changeTrip
     *
     * Get, validate and check user input data to change a trip and then call
     * the relevant function to perform the change
     *
     * @see \library\Trip->deleteTrip()
     */
    private function changeTrip() {
        // Get and validate user input data
        $errors = [];
        $tripid = Input::getInt('post', 'trip');
        $action = Input::getString('post', 'btn_action');

        // Make sure a trip and an action have been selected
        $tripid === FALSE ?
                Application::appendErrorString($errors, 'No trip selected') : NULL;
        // Currently the only possible change is deleting a trip
        $action !== 'trip_delete' ?
                Application::appendErrorString($errors, 'Action could not be completed') : NULL;

        $success = false;
        if (empty($errors)) {
            // If everything checks out try to perform the action
            $trip = new Trip();
            try{
                $success = $trip->deleteTrip($tripid);
            } catch (\Exception $e) {
                Application::appendErrorString($errors, ExceptHandler::getMessage($e));
            }
        }

        // Return a json encoded success or error message
        if ($success) {
                $done = 'Trip deleted!';
                $return = ['do' => 'display', 'success' => 'success', 'message' => $done,
                    'update_table' => 'delete_row', 'update_tr' => $tripid];
        } else {
            $return = ['do' => 'display', 'success' => 'error', 'message' => implode('', $errors)];
        }
        header('Content-Type: application/json');
        echo json_encode($return);
    }

    /**
     * addDeposit
     *
     * Get, validate and check user input, call a function to add the deposit
     * and return with json formatted data
     *
     * @todo There should be a check here, if the user actually exists
     *
     * @see \library\Trip->addDeposit()
     */
    private function addDeposit() {
        // Get and validate user input data
        $errors = [];
        $uid = Input::getInt('post', 'user_chosen');
        $amount = Input::getFloat('post', 'deposit_amount');

        // Make sure all fields have been filled out
        $amount === FALSE ?
                Application::appendErrorString($errors, 'Please enter a valid amount') : NULL;
        $uid === FALSE ?
                Application::appendErrorString($errors, 'Please enter a user') : NULL;

        $success = FALSE;
        if (empty($errors)) {
           // If everything checks out so far, try to add the deposit
           $trip = new Trip();
           try{
                $success = $trip->addDeposit($uid, $amount);
            } catch (\Exception $e) {
                Application::appendErrorString($errors, ExceptHandler::getMessage($e));
            }
        }

        // Return a json formatted success or error message
        if ($success) {
                $done = 'Deposit added!';
                $return = ['do' => 'display', 'success' => 'success', 'message' => $done];
        } else {
            $return = ['do' => 'display', 'success' => 'error', 'message' => implode('', $errors)];
        }
        header('Content-Type: application/json');
        echo json_encode($return);
    }

    /**
     * changeCar
     *
     * Get, check and validate user input for changing a car, then call the
     * appropriate function and return with a json formatted string
     *
     * @see \library\Trip->updateCar()
     */
    private function changeCar() {
        // Get and validate user input data
        $errors = [];
        $consumpt = Input::getFloat('post', 'consumpt');
        $fuel = Input::getInt('post', 'fuel');
        $seats = Input::getInt('post', 'seats');
        $action = Input::getString('post', 'btn_action');
        $uid = intval($_SESSION['user_id']);

        $success = FALSE;
        $trip = new Trip();
        try {
            switch ($action) {
                case 'delete' :
                    // In case the action is delete, most data can be set to the
                    // default. So no error checking necessary.
                    $fuel = NULL;
                    $consumpt = NULL;
                    $seats = NULL;
                    $delete = 1;
                    // If there were't any errors up to here, try to delete the car
                    empty($errors) ?
                        $success = $trip->updateCar($fuel, $uid, $consumpt, $seats, $delete) : NULL;
                        $done = 'Car deleted!';
                    break;
                case 'save' :
                    // In case the action is save make sure the input data makes sense
                    // All data must exists and have sane values.
                    $delete = 0;
                    if ($consumpt === FALSE || $fuel === FALSE || $seats === FALSE || $action === FALSE) {
                        Application::appendErrorString($errors, 'Please fill out all fields.');
                    }
                    $consumpt < 1 || $consumpt > 25 ?
                        Application::appendErrorString($errors, 'Please enter a valid value for fuel consumption.') : NULL;
                    $seats < 2 || $seats > 9 ?
                        Application::appendErrorString($errors, 'Please enter a valid number of seats.') : NULL;
                    empty($errors) ?
                        // If there weren't any errors, update the car with the new info
                        $success = $trip->updateCar($fuel, $uid, $consumpt, $seats, $delete) : NULL;
                        $done = 'Car saved!';
                    break;
                default :
                    // Output an error if the action was not in the list
                    Application::appendErrorString($errors, 'Action could not be completed (unknown)!');
                    break;
            }
        } catch (\Exception $e) {
            Application::appendErrorString($errors, ExceptHandler::getMessage($e));
        }

        // If everything went well up to here return with a json formatted
        // success or error message
        if ($success) {
                $return = ['do' => 'display', 'success' => 'success', 'message' => $done];
        } else {
            $return = ['do' => 'display', 'success' => 'error', 'message' => implode('', $errors)];
        }
        header('Content-Type: application/json');
        echo json_encode($return);
    }

    /**
     * changeFuelPrice
     *
     * Get, check and validate user input for changing fuel prices and then
     * insert the new fuel prices into the database by calling Trip->updateFuel
     *
     * @see \library\Trip->updateFuel()
     */
    private function changeFuelPrice() {
        // Get and validate user input data
        $errors = [];
        $changedfuels = Input::getString('post', 'fuellist');
        $fuellist = explode(',', $changedfuels);
        if (!empty($fuellist)) {
            foreach ($fuellist as $thisfuel) {
                $thisprice = Input::getFloat('post', $thisfuel);
                $fuelid = explode('_', $thisfuel);
                if ($thisprice !== FALSE) {
                    $allfuels[] = [$fuelid[0],$thisprice];
                } else {
                    Application::appendErrorString($errors, 'Invalid amount entered.');
                }
            }
        } else {
            Application::appendErrorString($errors, 'No fuel selected.');
        }

        $success = FALSE;
        $trip = new Trip();
        try {
            if (empty($errors)) {
                foreach ($allfuels as $fuel) {
                    $success = $trip->updateFuel($fuel[0], $fuel[1]);
                    if (!$success) {
                        break;
                    }
                }
            }
        } catch (\Exception $e) {
            Application::appendErrorString($errors, ExceptHandler::getMessage($e));
        }

        // If everything went well up to here return with a json formatted
        // success or error message
        if ($success) {
                $return = ['do' => 'display', 'success' => 'success', 'message' => 'Price changed successfully.'];
        } else {
            $return = ['do' => 'display', 'success' => 'error', 'message' => implode('', $errors)];
        }
        header('Content-Type: application/json');
        echo json_encode($return);
    }
}

