<?php
namespace library;
use Config;
/**
 * Update
 *
 * Provides functionality to update the application
 *
 * @package TravelExpenseManager
 *
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2016 Benedikt Grosch.
 *
 */
class Update {

    public static function checkUpdate() {
        if (self::checkDb() == 1) {
            self::updateDbV2();
        }
    }

    protected static function checkDb(){
        global $MysqlCon;
        $db_version = 1;
        $mysqli = $MysqlCon;
        $stmt = $mysqli->prepare("SELECT TABLE_NAME "
                               . "FROM INFORMATION_SCHEMA.TABLES "
                               . "WHERE TABLE_SCHEMA = ? "
                                     . "AND TABLE_NAME = 'app_version' "
                               . "LIMIT 1;");
        $stmt->bind_param('s', Config::$db_name);
        $stmt->execute();


        if (($result = $stmt->store_result()) && $stmt->num_rows == 0) {
            $db_version = 1;
        } else {
            $db_version = 2;
        }
        $stmt->close();

        // Return array of users
        return $db_version;
    }

    protected static function updateDbV2() {
        $trans_name = 'update_db_trans';

        global $MysqlCon;
        $mysqli = &$MysqlCon;
        $mysqli->autocommit(FALSE);
        $mysqli->begin_transaction(NULL, $trans_name);

        $alter_tables = "ALTER TABLE last_crons RENAME TO __arch_last_crons;"
                . "ALTER TABLE login_attempts RENAME TO __arch_login_attempts;"
                . "ALTER TABLE reset_requests RENAME TO __arch_reset_requests;"
                . "ALTER TABLE members RENAME TO __arch_members;"
                . "ALTER TABLE cartype RENAME TO __arch_cartype;"
                . "ALTER TABLE cars RENAME TO __arch_cars;"
                . "ALTER TABLE trips RENAME TO __arch_trips;";

        $mysqli->multi_query($alter_tables);

        if (self::checkDbErrors($mysqli, $alter_tables)) {
            $mysqli->rollback(NULL, $trans_name);
            $mysqli->close();
            return false;
        }

        $create_actions = "CREATE OR REPLACE TABLE app_actionlog ("
                            . "id int(11) unsigned NOT NULL AUTO_INCREMENT,"
                            . "atype varchar(50) NOT NULL DEFAULT '',"
                            . "user_id int(11) unsigned DEFAULT NULL,"
                            . "random_tok char(50) DEFAULT NULL,"
                            . "tok_used tinyint(1) unsigned NOT NULL DEFAULT '1',"
                            . "change_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,"
                            . "PRIMARY KEY (id),"
                            . "KEY action_type (atype)"
                        . ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";

        if (!($result = $mysqli->real_query($create_actions))) {
            $mysqli->rollback(NULL, $trans_name);
            $mysqli->close();
            return false;
        }

        $populate_actions = "INSERT INTO app_actionlog (atype, "
                              . "                 user_id, "
                              . "                 random_tok, "
                              . "                 tok_used, "
                              . "                 change_date)"
                              . "   SELECT   'cron' AS atype,"
                              . "             NULL AS user_id,"
                              . "             NULL AS random_tok,"
                              . "             1 AS tok_used,"
                              . "             FROM_UNIXTIME(time) AS change_date"
                              . "    FROM     __arch_last_crons"
                              . "    UNION ALL"
                              . "    SELECT   'login_fail' AS atype,"
                              . "             user_id,"
                              . "             NULL AS random_tok,"
                              . "             1 AS tok_used,"
                              . "             FROM_UNIXTIME(time) AS change_date"
                              . "    FROM     __arch_login_attempts"
                              . "    UNION ALL"
                              . "    SELECT   'pw_reset' AS atype,"
                              . "             user_id,"
                              . "             random_token AS tandom_tok,"
                              . "             used AS tok_used,"
                              . "             FROM_UNIXTIME(request_time) AS change_date"
                              . "    FROM     __arch_reset_requests"
                              . "    UNION ALL"
                              . "    SELECT   'remind_pay' AS atype,"
                              . "             id AS user_id,"
                              . "             NULL AS random_tok,"
                              . "             1 AS tok_used,"
                              . "             FROM_UNIXTIME(last_reminded) AS change_date"
                              . "    FROM     __arch_members"
                              . "    WHERE    last_reminded <> 0"
                              . "    ORDER BY change_date;";

        if (!($result = $mysqli->real_query($populate_actions))) {
            $mysqli->rollback(NULL, $trans_name);
            $mysqli->close();
            return false;
        }

        $create_tripfuel = "CREATE OR REPLACE TABLE trip_fuel ("
                         . "   id int(11) unsigned NOT NULL AUTO_INCREMENT,"
                         . "   ftype varchar(50) NOT NULL,"
                         . "   PRIMARY KEY (id)"
                         . " ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;"
                         . "INSERT INTO trip_fuel (ftype) VALUES ('Diesel');"
                         . "INSERT INTO trip_fuel (ftype) VALUES ('Benzin');"
                         . "INSERT INTO trip_fuel (ftype) VALUES ('LNG');";
        $mysqli->multi_query($create_tripfuel);

        if (self::checkDbErrors($mysqli, $create_tripfuel)) {
            $mysqli->rollback(NULL, $trans_name);
            $mysqli->close();
            return false;
        }

        $create_tripfuelprice = "CREATE OR REPLACE TABLE trip_fuel_price ("
                              . "    id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,"
                              . "    fuel INT(11) UNSIGNED NOT NULL,"
                              . "    price DECIMAL(10,4) UNSIGNED NOT NULL,"
                              . "    change_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
                                         ON UPDATE CURRENT_TIMESTAMP,"
                              . "    PRIMARY KEY (id),"
                              . "    KEY fuel_type (fuel),"
                              . "    CONSTRAINT fuel_name FOREIGN KEY (fuel)
                                         REFERENCES trip_fuel (id)"
                              . ") ENGINE=INNODB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;"
                              . "INSERT INTO trip_fuel_price (fuel, price) "
                              . "SELECT CASE id "
                              . "           WHEN 0 THEN (SELECT id FROM trip_fuel WHERE ftype = 'Benzin' LIMIT 1)"
                              . "           WHEN 1 THEN (SELECT id FROM trip_fuel WHERE ftype = 'Diesel' LIMIT 1)"
                              . "           WHEN 2 THEN (SELECT id FROM trip_fuel WHERE ftype = 'LNG' LIMIT 1)"
                              . "       END AS fuel,"
                              . "	pricing AS price "
                              . "FROM   __arch_cartype;";
        $mysqli->multi_query($create_tripfuelprice);

        if (self::checkDbErrors($mysqli, $create_tripfuelprice)) {
            $mysqli->rollback(NULL, $trans_name);
            $mysqli->close();
            return false;
        }

        $create_usert = "CREATE TABLE app_access ( "
                      . "  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, "
                      . "  access INT(11) NOT NULL, "
                      . "  title VARCHAR(250) NOT NULL DEFAULT '', "
                      . "  PRIMARY KEY (id) "
                      . ") ENGINE=INNODB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8; "
                      . "INSERT INTO app_access (id, access, title) "
                      . "VALUES "
                      . "	  (1,0,'Allgemein'), "
                      . "	  (2,1,'Mitglied'), "
                      . "	  (3,2,'Admin');"
                      . "CREATE TABLE user_user ("
                      . "    id int(11) unsigned NOT NULL AUTO_INCREMENT,"
                      . "    firstname varchar(50) NOT NULL DEFAULT '',"
                      . "    lastname varchar(50) NOT NULL DEFAULT '',"
                      . "    email varchar(50) NOT NULL DEFAULT '',"
                      . "    upassword char(128) NOT NULL DEFAULT '',"
                      . "    access tinyint(1) unsigned NOT NULL DEFAULT '0',"
                      . "    approved tinyint(1) unsigned NOT NULL DEFAULT '0',"
                      . "    deleted tinyint(1) unsigned NOT NULL DEFAULT '0',"
                      . "    change_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP "
                      . "        ON UPDATE CURRENT_TIMESTAMP,"
                      . "    PRIMARY KEY (id),"
                      . "    KEY email (email)"
                      . ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;"
                      . "INSERT INTO user_user (id, firstname, lastname, email, upassword, access, approved, deleted) "
                      . "SELECT id,"
                      . "    firstname,"
                      . "    lastname,"
                      . "    email,"
                      . "    password AS upassword,"
                      . "    CASE WHEN admin = 1 "
                      . "         THEN 3 "
                      . "         ELSE 2 "
                      . "    END AS access,"
                      . "    approved,"
                      . "    0 AS deleted "
                      . "FROM __arch_members;";
        $mysqli->multi_query($create_usert);

        if (self::checkDbErrors($mysqli, $create_usert)) {
            $mysqli->rollback(NULL, $trans_name);
            $mysqli->close();
            return false;
        }

        $create_usercar = "CREATE OR REPLACE TABLE trip_usercar ("
                        . "    id int(11) unsigned NOT NULL AUTO_INCREMENT,"
                        . "    fuel int(11) unsigned,"
                        . "    cowner int(11) unsigned NOT NULL,"
                        . "    consumpt DECIMAL(5,4),"
                        . "    seats int(5) unsigned,"
                        . "    deleted tinyint(1) unsigned NOT NULL DEFAULT 0,"
                        . "    change_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
                                   ON UPDATE CURRENT_TIMESTAMP,"
                        . "    PRIMARY KEY (id),"
                        . "    KEY user_car (cowner),"
                        . "    KEY car_fuel (fuel),"
                        . "    CONSTRAINT user_car FOREIGN KEY (cowner)
                                   REFERENCES user_user (id),"
                        . "    CONSTRAINT car_fuel FOREIGN KEY (fuel)
                                   REFERENCES trip_fuel (id)"
                        . ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;"
                        . "INSERT INTO trip_usercar (fuel, cowner, consumpt, seats) "
                        . "SELECT CASE kind"
                        . "           WHEN 0 THEN (SELECT id FROM trip_fuel WHERE ftype = 'Benzin' LIMIT 1)"
                        . "           WHEN 1 THEN (SELECT id FROM trip_fuel WHERE ftype = 'Diesel' LIMIT 1)"
                        . "           WHEN 2 THEN (SELECT id FROM trip_fuel WHERE ftype = 'LNG' LIMIT 1)"
                        . "       END AS fuel,"
                        . "       OWNER AS cowner,"
                        . "       literper AS consumpt,"
                        . "       seats AS seats "
                        . "FROM __arch_cars;";
        $mysqli->multi_query($create_usercar);

        if (self::checkDbErrors($mysqli, $create_usercar)) {
            $mysqli->rollback(NULL, $trans_name);
            $mysqli->close();
            return false;
        }

        $create_trip = "CREATE TABLE trip_trip ("
                     . "    id int(11) unsigned NOT NULL AUTO_INCREMENT,"
                     . "    car int(11) unsigned DEFAULT NULL,"
                     . "    distance decimal(10,4) unsigned NOT NULL,"
                     . "    driver_pays tinyint(1) unsigned NOT NULL DEFAULT '0',"
                     . "    fixed_price decimal(10,4) unsigned DEFAULT NULL,"
                     . "    change_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
                                ON UPDATE CURRENT_TIMESTAMP,"
                     . "    PRIMARY KEY (id),"
                     . "    KEY trip_car (car),"
                     . "    CONSTRAINT trip_car FOREIGN KEY (car) REFERENCES trip_usercar (id)"
                     . "    ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;"
                     . "CREATE TABLE trip_passenger ("
                     . "    id int(11) unsigned NOT NULL AUTO_INCREMENT,"
                     . "    trip int(11) unsigned NOT NULL,"
                     . "    user_id int(11) unsigned NOT NULL,"
                     . "    driver tinyint(1) unsigned NOT NULL DEFAULT '0',"
                     . "    change_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
                                ON UPDATE CURRENT_TIMESTAMP,"
                     . "    PRIMARY KEY (id),"
                     . "    KEY passenger (trip),"
                     . "    KEY user_passenger (user_id),"
                     . "    CONSTRAINT passenger FOREIGN KEY (trip)
                                REFERENCES trip_trip (id),"
                     . "    CONSTRAINT user_passenger FOREIGN KEY (user_id)
                                REFERENCES user_user (id)"
                     . ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";

        $mysqli->multi_query($create_trip);

        if (self::checkDbErrors($mysqli, $create_trip)) {
            $mysqli->rollback(NULL, $trans_name);
            $mysqli->close();
            return false;
        }

        $mysqli->real_query("SELECT trips.id AS id,"
                            . " trips.driver AS driver,"
                            . " car.id AS tcar,"
                            . " trips.passenger AS passenger,"
                            . " UNIX_TIMESTAMP(trips.trip_date) AS tdate,"
                            . " trips.price AS price,"
                            . " trips.driver_paid AS driver_paid"
                       . " FROM __arch_trips AS trips"
                            . " LEFT JOIN trip_usercar AS car ON trips.driver = car.cowner;");

        if (($result = $mysqli->store_result()) && $result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $tpassengers = explode(',', $row['passenger']);
                if ($row['tcar'] == NULL) {
                    $row['tcar'] = "NULL";
                }


                $query_rows[] = "INSERT INTO trip_trip (id, car, distance, driver_pays, fixed_price, change_date)"
                                    . " VALUES (" . $row['id'] . ", "
                                                  . $row['tcar'] . ", "
                                                  . "31.4, "
                                                  . $row['driver_paid'] . ", "
                                                  . $row['price'] . ", "
                                                  . "FROM_UNIXTIME(" . $row['tdate'] ."));";
                $query_rows[] = "INSERT INTO trip_passenger (trip, user_id, driver, change_date)"
                               . "    VALUES (" . $row['id']. ", "
                                                . $row['driver'] . ", "
                                                . "1, "
                                                . "FROM_UNIXTIME(" . $row['tdate'] ."));";
                foreach ($tpassengers as $pas) {
                    $query_rows[] = "INSERT INTO trip_passenger (trip, user_id, driver, change_date)"
                                  . "    VALUES (" . $row['id']. ", "
                                                   . $pas . ", "
                                                   . "0, "
                                                   . "FROM_UNIXTIME(" . $row['tdate'] ."));";
                }
            }
            $result->close();
        }
        $query = implode(" ", $query_rows);
        $mysqli->multi_query($query);

        if (self::checkDbErrors($mysqli, $query)) {
            $mysqli->rollback(NULL, $trans_name);
            $mysqli->close();
            return false;
        }

        $create_userdep = "CREATE TABLE trip_deposit ( "
                            . "id int(11) unsigned NOT NULL AUTO_INCREMENT, "
                            . "user_id int(11) unsigned NOT NULL, "
                            . "deposit decimal(10,4) NOT NULL, "
                            . "change_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, "
                            . "PRIMARY KEY (id), "
                            . "KEY deposit_user (user_id), "
                            . "CONSTRAINT deposit_user FOREIGN KEY (user_id) REFERENCES user_user (id) "
                            . ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;"
                        . "CREATE OR REPLACE VIEW trip_passengernum (trip, num_pas) AS "
                            . "SELECT pasc.trip, "
                            . "       COUNT(pasc.user_id) AS pas "
                            . "  FROM trip_passenger AS pasc "
                            . "       JOIN trip_trip trip ON pasc.trip = trip.id "
                            . " WHERE NOT (pasc.driver = 1 AND trip.driver_pays = 0) "
                            . "GROUP BY pasc.trip;"
                        . "INSERT INTO trip_deposit (user_id, deposit)"
                        . "SELECT id AS user_id, "
                            . "   new_dep AS deposit "
                        . "  FROM ( "
                            . "    SELECT mem.id AS id, "
                            . "           ROUND(- SUM(COALESCE( "
                            . "               CASE "
                            . "                   WHEN pas.driver = 1 "
                            . "                        AND tri.driver_pays = 0"
                            . "                   THEN tri.fixed_price "
                            . "                   WHEN pas.driver = 1 "
                            . "                        AND tri.driver_pays = 1 "
                            . "                   THEN tri.fixed_price - (tri.fixed_price / num.num_pas) "
                            . "                   WHEN pas.driver = 0 "
                            . "                   THEN - tri.fixed_price / num.num_pas "
                            . "               END, 0)) + (mem.deposit + mem.cost), 2) AS new_dep "
                            . "      FROM __arch_members AS mem "
                            . "           LEFT JOIN trip_passenger AS pas ON mem.id = pas.user_id "
                            . "           LEFT JOIN trip_trip AS tri ON pas.trip = tri.id "
                            . "           LEFT JOIN trip_passengernum AS num "
                            . "              ON pas.trip = num.trip "
                            . "  GROUP BY mem.id) AS newdep "
                            . "WHERE new_dep <> 0; ";

        $mysqli->multi_query($create_userdep);

        if (self::checkDbErrors($mysqli, $create_userdep)) {
            $mysqli->rollback(NULL, $trans_name);
            $mysqli->close();
            return false;
        }

        $create_views =   "CREATE OR REPLACE VIEW trip_passengercost (user_id, driver, trip, price) AS "
                        . "SELECT mem.id AS user_id, "
                        . "       pas.driver AS driver, "
                        . "       tri.id AS trip, "
                        . "       ROUND(CASE "
                        . "                 WHEN pas.driver = 1 "
                        . "                      AND tri.driver_pays = 0 "
                        . "                 THEN tri.fixed_price "
                        . "                 WHEN pas.driver = 1 "
                        . "                      AND tri.driver_pays = 1 "
                        . "                 THEN tri.fixed_price - (tri.fixed_price / num.num_pas) "
                        . "                 WHEN pas.driver = 0 "
                        . "                 THEN - tri.fixed_price / num.num_pas "
                        . "             END, 4) AS price "
                        . "FROM user_user AS mem "
                        . "     JOIN trip_passenger AS pas ON mem.id = pas.user_id "
                        . "     LEFT JOIN trip_trip AS tri ON pas.trip = tri.id "
                        . "     LEFT JOIN trip_passengernum AS num ON tri.id = num.trip; "
                        . "CREATE OR REPLACE VIEW trip_usercost (user_id, cost) AS "
                        . "  SELECT mem.id AS user_id, "
                        . "	 COALESCE(SUM(cos.price), 0) AS cost "
                        . "    FROM user_user AS mem "
                        . "         LEFT JOIN trip_passengercost AS cos ON mem.id = cos.user_id "
                        . "GROUP BY mem.id; "
                        . "CREATE OR REPLACE VIEW trip_userdeposit (user_id, deposit) AS "
                        . "  SELECT mem.id AS user_id, "
                        . "	 COALESCE(SUM(dep.deposit), 0) AS deposit "
                        . "    FROM user_user AS mem "
                        . "         LEFT JOIN trip_deposit AS dep ON mem.id = dep.user_id "
                        . "GROUP BY mem.id; "
                        . "CREATE OR REPLACE VIEW trip_userbalance (user_id, cost, deposit, total) AS "
                        . "SELECT mem.id AS user_id, "
                        . "       cos.cost AS cost, "
                        . "       dep.deposit AS deposit, "
                        . "       dep.deposit + cos.cost AS total "
                        . "  FROM user_user AS mem "
                        . "       JOIN trip_usercost AS cos ON mem.id = cos.user_id "
                        . "       JOIN trip_userdeposit AS dep ON mem.id = dep.user_id; ";

        $mysqli->multi_query($create_views);
        if (self::checkDbErrors($mysqli, $create_views)) {
            $mysqli->rollback(NULL, $trans_name);
            $mysqli->close();
            return false;
        }

        $create_actionhandl = "CREATE TABLE app_actions ( "
                            . "     id int(11) unsigned NOT NULL AUTO_INCREMENT, "
                            . "     action varchar(50) NOT NULL,"
                            . "     handler varchar(50) NOT NULL, "
                            . "     method varchar(50) NOT NULL, "
                            . "     access int(11) unsigned NOT NULL DEFAULT 0, "
                            . "     PRIMARY KEY (id), "
                            . "     KEY action (action)"
                            . ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8; "
                            . "INSERT INTO app_actions (action, handler, method, access) "
                            . "VALUES "
                            . "     ('login', 'AppActions','login',1), "
                            . "     ('logout', 'AppActions','logout',1), "
                            . "     ('register', 'AppActions','userRegister',1), "
                            . "     ('user_pwreset', 'AppActions','userPwReset',1), "
                            . "     ('user_pwupdate', 'AppActions','userPwUpdate',2), "
                            . "     ('user_approve', 'AppActions','userApprove',3), "
                            . "     ('user_update', 'AppActions','userUpdate',3),"
                            . "     ('user_pwforgot', 'AppActions', 'userPwForgot', 1),"
                            . "     ('user_change', 'AppActions', 'changeUser', 3),"
                            . "     ('trip_add', 'TripActions', 'addTrip', 2),"
                            . "     ('trip_deposit', 'TripActions', 'addDeposit', 3),"
                            . "     ('trip_change', 'TripActions', 'changeTrip', 2),"
                            . "     ('change_car', 'TripActions', 'changeCar', 2); ";

        $mysqli->multi_query($create_actionhandl);

        if (self::checkDbErrors($mysqli, $create_actionhandl)) {
            $mysqli->rollback(NULL, $trans_name);
            $mysqli->close();
            return false;
        }

        $create_versiont = "CREATE TABLE app_version ( "
                           . " id int(11) unsigned NOT NULL AUTO_INCREMENT, "
                           . " version char(4) NOT NULL DEFAULT '', "
                           . " change_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, "
                           . " PRIMARY KEY (id) "
                           . ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8; "
                           . "INSERT INTO app_version (version) VALUES ('1.10');";

        $mysqli->multi_query($create_versiont);
        if (self::checkDbErrors($mysqli, $create_userdep)) {
            $mysqli->rollback(NULL, $trans_name);
            $mysqli->close();
            return false;
        }

        $mysqli->commit(NULL, $trans_name);

    }

    protected static function checkDbErrors($mysqli, $query) {
        $errors = FALSE;
        do {
            if (!($result = $mysqli->next_result())) {
                $errors = TRUE;
                echo "An Error was encountered during the update. The error was: <br />";
                echo $mysqli->error;
                echo "<br />The query was: <br />";
                var_dump($query);
                break;
            }
        } while ($mysqli->more_results());

        return $errors;
    }
}
