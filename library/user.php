<?php
namespace library;
use Config;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * User
 *
 * Manage actions concerning users, such as register, updating passwords and
 * changing their membership status. Also allows to get user information from
 * the database.
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */
class User {

    /**
     * @var int The user id
     */
    private $id;

    /**
     * @var string Users firstname
     */
    private $firstname;

    /**
     * @var string Users lastname
     */
    private $lastname;

    /**
     * @var string Users email address
     */
    private $email;

    /**
     * @var string Hashed password, only set when it should be updated
     */
    private $pw_hash = NULL;

    /**
     * @var int Access level for the user
     */
    private $access = 0;

    /**
     * @var int Approval status
     */
    private $approved = 0;

    /**
     * @var int Deletion status (0,1)
     */
    private $deleted = 0;

    /**
     * @var string Date of last change
     */
    private $change_date;

    /**
     * @var boolean Does the user exist?
     */
    private $exists = false;

    /**
     * initializeByEmail
     *
     * Get all user information from the database using the email address and
     * store it inside the object.
     *
     *
     * @global \Mysqli $MysqlCon Mysql Database connection
     * @param string $email Email Address of the user to initialize
     * @return boolean  True if user was found, false otherwise
     * @throws Exception ERROR_DATABASE_SELECT: Could not get user from database
     */
    public function initializeByEmail($email) {
        global $MysqlCon;
        $mysqli = &$MysqlCon;
        // Prepare the query to select the user from database
        $query = "SELECT id, firstname, lastname, email, access, approved, "
               . "       deleted, UNIX_TIMESTAMP(change_date)"
               . "  FROM user_user"
               . " WHERE email = ?"
               . " LIMIT 1;";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($id, $firstname, $lastname, $email, $access, $approved,
                $deleted, $changed);
        $stmt->fetch();

        if ($stmt->errno != 0) {
            // If there was an error, throw an Exception
            throw new Exception("mysql: Could not get user from databse", ERROR_DATABASE_SELECT);
        }

        if ($stmt->num_rows > 0) {
            // If everything went fine up to here, store all the values and
            // make sure they have the correct data types
            $this->exists = true;
            $this->id = intval($id);
            $this->firstname = $firstname;
            $this->lastname = $lastname;
            $this->email = $email;
            $this->access = intval($access);
            $this->approved = boolval($approved);
            $this->deleted = boolval($deleted);
            $this->change_date = intval($changed);
        } else {
            $this->exists = false;
        }
        // Return True if the user exists, false otherwise
        return $this->exists;
    }

    /**
     * initializeById
     *
     * Get all user information from the database using the user id and
     * store it inside the object.
     *
     *
     * @global \Mysqli $MysqlCon Mysql Database connection
     * @param int $uid User id of the user to initialize
     * @return boolean  True if user was found, false otherwise
     * @throws Exception ERROR_DATABASE_SELECT: Could not get user from database
     */
    public function initializeById($uid) {
        global $MysqlCon;
        $mysqli = &$MysqlCon;
        // Prepare the database query to select the user from the db
        $query = "SELECT id, firstname, lastname, email, access, approved, "
               . "       deleted, UNIX_TIMESTAMP(change_date)"
               . "  FROM user_user"
               . " WHERE id = ?"
               . " LIMIT 1;";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('s', $uid);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($id, $firstname, $lastname, $email, $access, $approved,
                $deleted, $changed);
        $stmt->fetch();

        if ($stmt->errno != 0) {
            // Throw an error when the database returned an error
            throw new Exception("mysql: Could not get user from databse", ERROR_DATABASE_SELECT);
        }
        if ($stmt->num_rows > 0) {
            // If the user exists store all values in the object and make sure
            // the have the correct data types
            $this->exists = true;
            $this->id = intval($id);
            $this->firstname = $firstname;
            $this->lastname = $lastname;
            $this->email = $email;
            $this->access = intval($access);
            $this->approved = boolval($approved);
            $this->deleted = boolval($deleted);
            $this->change_date = intval($changed);
        } else {
            $this->exists = false;
        }
        // Return true if user was found, false otherwise
        return $this->exists;
    }

    /**
     * getId
     *
     * Get the user id
     *
     * @return int User id
     */
    public function getId() {
        return $this->id;
    }
    /**
     * getName
     *
     * Get the users full name
     *
     * @return string Users full name
     */
    public function getName() {
        return $this->firstname . ' ' . $this->lastname;
    }

    /**
     * getFirstname
     *
     * Get the users firstname
     *
     * @return string Users firstname
     */
    public function getFirstname() {
        return $this->firstname;
    }

    /**
     * getLastname
     *
     * Get the users lastname
     *
     * @return string Users lastname
     */
    public function getLastname() {
        return $this->lastname;
    }

    /**
     * getEmail
     *
     * Get the users email address
     *
     * @return string Users email address
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * getUsersList
     *
     * Get a list of all users that are not currently deleted and return them
     * as an associative array
     *
     * @todo Add some Error handling for the database connection
     *
     * @global \Mysqli $MysqlCon Mysql connector object
     * @return Array Associative array of user information from database
     */
    public function getUsersList() {
        global $MysqlCon;
        $mysqli = &$MysqlCon;
        // Prepare the query to select all users from database
        $query =  "  SELECT id, CONCAT(firstname, ' ', lastname) AS mname, email, access, approved "
                . "    FROM user_user "
                . "   WHERE deleted = 0 "
                . "ORDER BY lastname;";
        $stmt = $mysqli->prepare($query);
        $stmt->execute();
        $stmt->store_result();
        $members = [];
        if ($stmt->num_rows >= 1) {
            $stmt->bind_result($id, $mname, $email, $access, $approved);
            // If some rows were found, insert them into the assoc array
            while ($stmt->fetch()) {
                $row['id'] = $id;
                $row['mname'] = $mname;
                $row['email'] = $email;
                $row['access'] = $access;
                $row['approved'] = $approved;

                $members[] = $row;
            }
            $ret = $members;
        } else {
            $ret = FALSE;
        }
        $stmt->close();
        return $ret;
    }

    /**
     * Register new user
     *
     * Method to verify input data, send a new user email to the admin for
     * enabling them to activate the user and save the user.
     *
     * @todo When an admin registers the user manually no additional activiation
     *       should be necessary. It should happen automatically.
     *
     * @param string $firstname Firstname of the user trying to register
     * @param string $lastname Lastname of the user trying to register
     * @param string $email E-Mail address to be registered
     * @param string $pw_hash Hashed password to be written to the db
     * @return bool true if successfull, false otherwise
     * @throws \Exception Rethrows Exception from sending email or saving user
     */
    public function register($firstname, $lastname, $email, $pw_hash) {
        // Store all the information in the user object
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->pw_hash = $pw_hash;
        $this->access = 2;
        $this->approved = false;
        $this->deleted = false;

        try {
            // Try to send an email to the admins
            $msg = 'User ' . $this->firstname . ' ' . $this->lastname
                . ' has registered and must be approved.';
            Application::sendEmail(Config::$admin_email, 'New User', $msg);


            // If no errors were found, add the new user and return a positive answer
            $this->saveUser();
            $return = true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
        return $return;
    }

    /**
     * forgotPW
     *
     * Generate a token for resetting the password, store it in the db and send
     * it to the user.
     *
     * @global \Mysqli $MysqlCon Mysql connector object
     * @return boolean True on success
     * @throws \Exception ERROR_DATABASE_INSERT: Could not insert token
     */
    public function forgotPW() {
        global $MysqlCon;
        $mysqli = &$MysqlCon;

        // Generate a cryptographically safe token with 50 characters
        $token = substr(bin2hex(random_bytes(128)), 0, 50);
        $query = "INSERT INTO app_actionlog "
               . "(atype, user_id, random_tok, tok_used) "
               . "VALUES ('pw_reset', ?, ?, 0);";
        // Insert the random token and current time into the database
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('is', $this->id, $token);
        $stmt->execute();
        if ($stmt->errno != 0) {
            // Throw an Exception if something went wrong
            throw new \Exception('mysqli: Could not insert token into database', ERROR_DATABASE_INSERT);
        }
        $stmt->close();

        // Prepare the email to send to the user for resetting the PW
        $link_attrib = '&id=' . $this->id
                    .  '&email=' . $this->email
                    .  '&token=' . $token;

        $msg = 'Hey ' . $this->firstname . ',<br /><br />Please use this  '
            .  'link to reset your password:<br />'
            .  '<a href="http://' . Config::$page_domain . '/index.php?'
            .  'site=reset-password'
            .  $link_attrib . '">http://' . Config::$page_domain . '/index.php?'
            .  'site=reset-password'
            .  $link_attrib . '</a>'
            .  '<br /><br/>Kind regards<br />Travel Expense Manager';
        Application::sendEmail($this->email, 'Reset Password', $msg);

        return true;
    }

    /**
     * resetPW
     *
     * Update the token to become invalid and insert the new password for the
     * user.
     *
     * @global \Mysqli $MysqlCon Mysql connector object
     * @param string $new_pw Hashed new password
     * @param int $tok_id Id of the token in the database
     * @return boolean True if save was successful
     * @throws \Exception ERROR_DATABASE_UPDATE: Could not update token
     */
    public function resetPW($new_pw, $tok_id) {
        global $MysqlCon;
        $mysqli = &$MysqlCon;
        // Store the new password hash in this object
        $this->pw_hash = password_hash($new_pw, PASSWORD_DEFAULT);
        // Prepare the query to update the token to used
        $query = "UPDATE app_actionlog SET tok_used = 1 WHERE id = ?;";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('i', $tok_id);
        $stmt->execute();

        if ($stmt->errno !== 0) {
            // Throw an exception if something went wrong with the update
            throw new \Exception('Mysql: Could not update PW Reset Token', ERROR_DATABASE_UPDATE);
        }

        // Return with the return value of saveUser
        return $this->saveUser();
    }

    /**
     * changeAccess
     *
     * Changes the access level and saves the user info to the database
     *
     * @todo Add some error handling
     *
     * @param int $access New access level for the user
     * @return boolean True on success
     */
    public function changeAccess($access) {
        $this->access = $access;
        $this->saveUser();
        return true;
    }

    /**
     * approve
     *
     * Changes the approval status of a user to 1 and saves user info to db.
     * Also sends an email to the user to notify them of their approval.
     *
     * @todo Add some error handling
     *
     * @return boolean True on success
     */
    public function approve() {
        if ($this->approved !== 1) {
            $this->approved = 1;
            $this->saveUser();

            $msg = 'Hey ' . $this->firstname . ',<br /><br />Your account for the '
                . 'trave expense manager has been approved. You can now login and '
                . 'manage your trips. '
                . '<br /><br/>Take care, <br />Travel Expense Manager';
            Application::sendEmail($this->email, 'Travel Expense Manager Account Approved', $msg);
        }
        return true;
    }

    /**
     * delete
     *
     * Set the delete flag for the user to 1 and saves user info to db.
     *
     * @todo Add some error handling
     *
     * @return boolean True on success
     */
    public function delete() {
        $this->deleted = 1;
        $this->saveUser();
        return true;
    }

    /**
     * update
     *
     * Updates user info, namely firstname, lastname and email. First stores
     * them in the object and then saves everything to the db.
     *
     * @todo Add some error handling
     *
     * @param string $firstname Users firstname
     * @param string $lastname Users lastname
     * @param string $email Users email address
     * @return boolean True on success
     */
    public function update($firstname, $lastname, $email) {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->saveUser();
        return true;
    }

    /**
     * updatePw
     *
     * Hash a new password and save it into the database
     *
     * @param string $new_pw New password
     * @return boolean True on success
     */
    public function updatePw($new_pw) {
        $this->pw_hash = password_hash($new_pw, PASSWORD_DEFAULT);
        $this->saveUser();
        return true;
    }

    /**
     * saveUser
     *
     * Save user to the database with different queries depending on the type
     * of changes that were made.
     *
     * @return boolean Success (true) or failure (false)
     */
    protected function saveUser() {
        global $MysqlCon;
        $mysqli = &$MysqlCon;

        if ($this->exists === false) {
            // If the user doesn't exist, insert it
            $query = "INSERT INTO user_user (firstname, "
                   . "                    lastname, "
                   . "                    email, "
                   . "                    upassword, "
                   . "                    access, "
                   . "                    approved, "
                   . "                    deleted) "
                   . "VALUES (?, ?, ?, ?, ?, ?, ?);";
            $stmt = $mysqli->prepare($query);
            $stmt->bind_param('ssssiii', $this->firstname, $this->lastname,
                    $this->email, $this->pw_hash, $this->access,
                    $this->approved, $this->deleted);
        } elseif ($this->exists === true && $this->pw_hash !== NULL) {
            // If the user exists and a new password was given, update all info
            // including the password
            $query = "UPDATE user_user "
                   . "   SET firstname = ?, "
                   . "       lastname  = ?, "
                   . "       email     = ?, "
                   . "       upassword = ?, "
                   . "       access    = ?, "
                   . "       approved  = ?, "
                   . "       deleted   = ? "
                   . " WHERE id = ?;";
            $stmt = $mysqli->prepare($query);
            $stmt->bind_param('ssssiiii', $this->firstname, $this->lastname,
                    $this->email, $this->pw_hash, $this->access, $this->approved,
                    $this->deleted, $this->id);
        } elseif ($this->exists === true) {
            // If the user exists and no new password was given, update all info
            // excluding the password
            $query = "UPDATE user_user "
                   . "   SET firstname = ?, "
                   . "       lastname  = ?, "
                   . "       email     = ?,"
                   . "       access    = ?, "
                   . "       approved  = ?, "
                   . "       deleted   = ? "
                   . " WHERE id = ?;";
            $stmt = $mysqli->prepare($query);
            $stmt->bind_param('sssiiii', $this->firstname, $this->lastname,
                    $this->email, $this->access, $this->approved,
                    $this->deleted, $this->id);
        } else {
            // If none of the above options is true, trigger an error
            trigger_error("User: Action not possible or defined", E_USER_ERROR);
        }

        $stmt->execute();
        if ($stmt->errno != 0) {
            // Throw an exception if something went wrong during the update
            throw new \Exception("mysqli: Could not insert or update user!", ERROR_DATABASE_INSERT);
        }

        $stmt->close();
        return true;
    }
}
