<?php
namespace library;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * UserAuth
 *
 * Provides functions for checking the users authentication, approximately
 * using equal time
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 */
class UserAuth {

    /**
     * @var int User id
     */
    protected $id = 0;

    /**
     * @var string Password
     */
    protected $pw;

    /**
     * @var string Users firstname
     */
    protected $firstname;

    /**
     * @var string Users lastname
     */
    protected $lastname;

    /**
     * @var string Users email
     */
    protected $email;

    /**
     * @var int Access level of this user
     */
    protected $access;

    /**
     * @var boolean Bool value, user approved?
     */
    protected $approved;

    /**
     * @var boolean Bool value, user deleted?
     */
    protected $deleted;

    /**
     * @var boolean Bool value, user exists?
     */
    protected $user_exists;

    /**
     * loginCheck
     *
     * Checks passed login information against the database
     *
     * @todo Brute force recognition does not recognize all possible login failures.
     *       Database entry is not made when user does not exist (ip logging...)
     *
     * @param  string $email    Email Address of the user to be checked
     * @param  string $password User input password
     * @return boolean          True (valid login), False (invalid login)
     */
    protected function loginCheck($email, $password) {
        try {
            $this->getUserLoginData($email);
        } catch (Exception $e) {
            ExceptHandler::databaseError($e);
        }
        $access = 0;
        $checks = 0;

        $checks++;
        $this->user_exists ? $access++ : $access--; // Check if user exists in database

        $checks++;
        $this->approved ? $access++ : $access--;    // Check if user has been approved

        $checks++;
        !$this->deleted ? $access++ : $access --;    // Check if user has been deleted

        $checks++;
        $this->brute ? $access++ : $access--;       // Check if there was a brute force attempt

        // We need to make sure that non existent users take equal time to check
        // as existing users.
        $checks++;
        $need_rehash = password_needs_rehash($this->pw, PASSWORD_DEFAULT);
        if ($need_rehash) {
            password_hash("rpifkljbasdo8z0rpwpqeirzpfnsl", PASSWORD_DEFAULT) ? $access-- : $access--;
        } else {
            //Check user password against database
            password_verify($password, $this->pw) ? $access++ : $access--;
        }

        $success = ($checks === $access) ? TRUE : FALSE;  // Using all test results, was login successful?

        try {
            // Call the rehash function but only actually perform a rehash when needed.
            $success && $need_rehash ? $this->rehashPassword($this->id, $password) : NULL;
        } catch (Exception $e) {
            ExceptHandler::databaseError($e);
        }

        try {
            $this->storeLoginAttempt($this->id, $success);
        } catch (Exception $e) {
            ExceptHandler::databaseError($e);
        }

        return $success;
    }

    /**
     * getUserId
     *
     * Get the user Id, if it has been stored already from property, otherwise
     * from database.
     *
     * @param string $email E-Mail address (only when getting from database)
     * @return int User id
     * @throws \Exception ERROR_DATABASE_SELECT: Could not get user from database
     */
    protected function getUserId ($email = NULL) {
        if ($this->id === 0) {
            if ($email === NULL) {
                trigger_error("UserAuth::getUserId: Incorrect paramenter count", E_USER_ERROR);
            }

            // Get the user id for the given email from the database
            global $MysqlCon;
            $mysqli = &$MysqlCon;
            $query = "SELECT us.id FROM user_user AS us WHERE us.email = ? LIMIT 1;";
            $stmt = $mysqli->prepare($query);
            $stmt->bind_param('s', $email);
            $stmt->execute();
            if (!$stmt->store_result()) {
                // Throw an exception if anything went wrong
                throw new \Exception("mysqli: Could not get user from Database", ERROR_DATABASE_SELECT);
            }
            $stmt->bind_result($user_id);
            $stmt->fetch();
            $stmt->close();
            // Store the user id in this object
            $this->id = $user_id;
        } else {
            // If it was there already just use the id from this object
            $user_id = $this->id;
        }
        // Return the user id
        return $user_id;
    }

    /**
     * Get user login data for a given email address
     *
     * @param  string $email Email address of the user to be retrieved
     * @throws \Exception ERROR_DATABASE_SELECT: Could not get user from database
     */
    private function getUserLoginData ($email) {
        global $MysqlCon;
        $mysqli = &$MysqlCon;
        $brute_max_attempts = 5;            // Set some defaults for brute force recognition
        $brute_check_duration = 2*60*60;

        // Get user information from database
        $query = "SELECT us.id, us.upassword, us.firstname, us.lastname, us.email, us.access, us.approved, us.deleted, "
               . "       CASE "
               . "           WHEN fail.cnt >= ? THEN 0 "
               . "           ELSE 1 "
               . "       END AS brute "
               . "  FROM user_user AS us "
               . "       LEFT JOIN (  SELECT act.user_id AS user_id, COUNT(act.user_id) AS cnt "
               . "                      FROM app_actionlog AS act "
               . "                     WHERE act.atype = 'login_fail' "
               . "	                     AND UNIX_TIMESTAMP(act.change_date) > "
               . "                               UNIX_TIMESTAMP() - ? "
               . "                  GROUP BY act.user_id) AS fail "
               . "           ON us.id = fail.user_id "
               . " WHERE email = ? "
               . " LIMIT 1;";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('iis', $brute_max_attempts, $brute_check_duration, $email);
        $stmt->execute();
        if (!$stmt->store_result()) {
            // Throw an exception if something went wrong
            throw new \Exception ("mysqli: Could not get user from Database", ERROR_DATABASE_SELECT);
        }
        // Get variables from result.
        $stmt->bind_result($user_id, $password, $this->firstname,
                $this->lastname, $this->email, $access, $approved,
                $deleted, $brute);
        $stmt->fetch();
        $stmt->close();

        // Store everything in this object
        $this->id = intval($user_id);
        $this->access = intval($access);
        $this->approved = boolval($approved);
        $this->deleted = boolval($deleted);
        $this->brute = boolval($brute);

        // Make sure everything is done in equal time
        if ($this->id === 0) {
            $this->user_exists = FALSE;
            $this->pw = "23o894zhewliabt298345haswlkdfjnwbph5t";
        } else {
            $this->user_exists = TRUE;
            $this->pw = $password;
        }
    }

     /**
      * storeLoginAttempt
      *
      * Store login attempts in the Database
      *
      * @todo Also store IP Address and Forwarded For to make sure attempts
      *       for non existing users are also blocked
      *
      * @param  int $user_id Id of the user that tried to login
      * @param  boolean TRUE if login attempt was successful otherwise FALSE
      * @throws Exception 2102: Inserting failed login attempt failed
      */
    private function storeLoginAttempt ($user_id, $success) {
        global $MysqlCon;
        $mysqli = &$MysqlCon;

        // Prepare the query to execute whether login was successful or not
        $atype = $success ? 'login_success' : 'login_fail';
        $query = "INSERT INTO app_actionlog (atype, user_id) "
               . "VALUES(?, ?)";

        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('si', $atype, $user_id);
        $stmt->execute();

        if ($stmt->errno != 0) {
            // Throw an exception when something went wrong
            throw new Exception("mysqli: Inserting failed login attempt failed", ERROR_DATABASE_INSERT);
        }
        $stmt->close();
    }

    /**
     * rehashPassword
     *
     * Rehash password if it is necessary. Using this kind of fails the equal
     * time paradigm though.
     *
     * @param  int     $user_id  User id
     * @param  string  $password Users given password
     */
    private function rehashPassword($user_id, $password) {
        $hashed_pw = password_hash($password, PASSWORD_DEFAULT);
        global $MysqlCon;
        $mysqli = &$MysqlCon;

        // Prepare the statement to update the user data
        $stmt = $mysqli->prepare("UPDATE user_user "
                               . "   SET upassword = ? "
                               . " WHERE id = ?;");
        $stmt->bind_param('si', $hashed_pw, $user_id);
        $stmt->execute();
        if ($stmt->errno != 0) {
            // Throw an exception if anything went wrong
            throw new Exception("mysqli: Could not update user password", ERROR_DATABASE_UPDATE);
        }
        $stmt->close();
    }
}
