<?php
namespace library;
use Config;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * ViewController
 *
 * Manage views and provide all the relevant data for properly displaying them
 * Uses info from config file to setup the menu structure
 *
 * @todo Put information about views in database
 * @todo Implement/Improve fine grained access control
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */
class ViewController {

    /**
     * @var string Name of the requested site
     */
    public $active_site;

    /**
     * @var int Config menu id of the active site
     */
    public $active_site_id;

    /**
     * @var int The current users login status
     */
    public $login_status;

    /**
     * @var string  Name of the entire website
     */
    public $site_name;

    /**
     * @var string  (HTML) title of the entire website
     */
    public $page_title;

    /**
     * @var string  Description of the page
     */
    public $page_description;

    /**
     * @var array   Contains paths to all stylesheets that need to be included
     */
    public $stylesheet_path = ['css/bootstrap.min.css', 'css/chosen.css', 'css/jquery-ui.min.css'];

    /**
     * @var array   Contains paths to javascript files to be included in the page header
     */
    public $javascript_path_top = [];

    /**
     * @var array   Contains paths to javascript files to be included at the bottom of the page
     */
    public $javascript_path_bottom = ['js/jquery.min.js',
                                      'js/bootstrap.min.js',
                                      'js/chosen.jquery.min.js',
                                      'js/jquery-ui.min.js',
                                      'js/ie10-viewport-bug-workaround.js',
                                      'js/actions.js'];

    /**
     * @var bool    Defines if the site can be found by search engines
     */
    public $search_directive = false;

    /**
     * @var array   All data needed to print a view as returned by that views preprateView() method
     */
    protected $session;

    /**
     * @var Mixed    Object of the handler class for this view
     */
    protected $handler;

    /**
     * __construct
     *
     * Collect information about the requested view, access level and additional
     * data and store it in this object for later use
     *
     * @param int $login_status The current users login status
     */
    public function __construct($login_status) {
        $this->login_status = $login_status;

        // Determine which site to load
        $view_ident = $this->getViewIdent(Input::getURLParam('site'));
        $this->active_site = $view_ident['name'];
        $this->active_site_id = $view_ident['id'];

        // Get view data but do not output yet
        $this->getViewData();
    }

    /**
     * perform
     *
     * Store session object in this object and load the template
     *
     * @param \library\Session $session The session object
     */
    public function perform(&$session) {
        $this->session = $session;
        $this->loadTemplate();
    }

    /**
     * getViewIdent
     *
     * Check users login status against required login status and return a matching
     * view. Fallback depending on login status if view does not exist.
     *
     * @param string $active_site Currently requested site
     * @return array View id and name
     * @see Config::$menu
     */
    private function getViewIdent($active_site){
        $found = false;

        // Search requested view in Config::$menu
        while (!$found) {
            foreach (Config::$menu as $key => $view) {
                // If view name was found check also against login status
                if ($active_site == $view['name'] &&
                        $view['login'] <= $this->login_status) {
                    $view_ident = array('id' => $key, 'name' => $view['name']);
                    $found = true;
                }
            }
            // In case the view wasn't found or login status didn't match choose
            // fallback depending of login status. (Must exist in Config::$menu,
            // otherwise results in endless loop!)
            if (!$found && $this->login_status >= 2) {
                $active_site = 'mytrips';
            } else {
                $active_site = 'login';
            }
        }
        return $view_ident;
    }

    /**
     * getViewData
     *
     * Gets all data for showing the view, from standard values in this object
     * and from Config
     *
     * @see \Config
     */
    private function getViewData() {
        // Get title, stylesheets and javascript files
        $this->page_title = $this->getName();
        $stylesheets      = $this->getStylesheets();
        $js_path_top      = $this->getJavascript('top');
        $js_path_bottom   = $this->getJavascript('bottom');
        $this->site_name  = Config::$sitename;
        $this->page_title = Config::$sitename . ' - ' . $this->page_title;
        $this->page_description = Config::$page_description;

        // Set search engine header
        if (Config::$search_engine === FALSE) {
            $this->search_engine = 'noindex,nofollow';
        } else {
            $this->search_engine = 'index,follow';
        }

        // Set favicon and js and stylesheet paths
        $this->fav_icon = Config::$favicon;
        $this->stylesheet_path = array_merge($this->stylesheet_path, $stylesheets);
        $this->javascript_path_top = array_merge($this->javascript_path_top, $js_path_top);
        $this->javascript_path_bottom = array_merge($this->javascript_path_bottom, $js_path_bottom);
    }

    /**
     * getName
     *
     * Get title of the current view
     *
     * @return string Title of the current view
     */
    private function getName() {
        return Config::$menu[$this->active_site_id]['title'];
    }

    /**
     * getStylesheets
     *
     * Gets stylesheets for current view and formats them for output
     *
     * @return array All stylesheets required by the current view
     */
    private function getStylesheets() {
        $stylesheet_path = [];

        // Get all stylesheets for this view from Config::menu
        $item = Config::$menu[$this->active_site_id];
        foreach ($item['stylesheets'] as $stylesheet) {
            array_push($stylesheet_path, 'css/' . $stylesheet . '.css');
        }
        return $stylesheet_path;
    }

    /**
     * Get javascript files
     *
     * Gets all javascript files for current view and formats them for output
     *
     * @return array All javascript files required by the current view
     */
    private function getJavascript($where) {
        $item = Config::$menu[$this->active_site_id];
        // Separate between top and bottom js
        if ($where === 'top') {
            $pos = 'js_top';
        } else {
            $pos = 'js_bottom';
        }
        // Get all Javascript files from Config::menu
        $javascript_path = [];
        if (array_key_exists($pos, $item)) {
            foreach ($item[$pos] as $javascript) {
                $javascript_path[] =  'js/' . $javascript . '.js';
            }
        } else {
            $javascript_path = [];
        }
        return $javascript_path;
    }

    /**
     * loadTemplate
     *
     * Load the template which will do further inclusions.
     */
    private function loadTemplate() {
        require_once 'template.php';
    }

    /**
     * showMenu
     *
     * Creates a menu for the requested page in the final output format, that
     * can be directly included in the view.
     */
    protected function showMenu() {
        // Prepare arrays for left and right menus
        $menu_left = array();
        $menu_right = array();
        $active_site = $this->active_site;
        $login_status = $this->login_status;

        // Iterate all menu items and create HTML
        foreach (Config::$menu as $link) {
            if (array_key_exists('link', $link) && $link['login'] <= $login_status) {
                // Item contains link and Users login status is sufficient
                if ($link['name'] != $active_site) {
                    // Item is not the active site and should be shown normally
                    $htmlstring = '<li><a href="index.php?site=' . $link['name']
                            . '">' . $link['link'] . '</a></li>';
                } else {
                    // Item is the the active site and should be shown as such
                    $htmlstring = '<li class="active"><a href="index.php?site='
                            . $link['name'] . '">' . $link['link'] . '</a></li>';
                }
                if ($link['side'] === 'left') {
                    // Add created link to left menu
                    array_push($menu_left, $htmlstring);
                } elseif ($link['side'] ===  'right') {
                    // Add created link to right menu
                    array_push($menu_right, $htmlstring);
                }
            }
        }

        // The right menu should additionally contain a logout link
        array_push($menu_right, '<li><a href="index.php?action=logout">Logout</a></li>');

        // Output menu items as HTML <ul> list. Left menu first
        echo '<ul class="nav navbar-nav">';
        foreach ($menu_left as $item) {
            echo $item;
        }
        echo '</ul>';

        // Output menu items as HTML <ul> list. Right menu second
        echo '<ul class="nav navbar-nav navbar-right">';
        foreach ($menu_right as $item) {
            echo $item;
        }
        echo '</ul>';
    }

    /**
     * showView
     *
     * Calls this->loadView with the active site name to load the view
     *
     * @see ViewController::loadView()
     */
    public function showView() {
        $this->loadView($this->active_site);
    }

    /**
     * loadView
     *
     * Load view definition file for further view preparation
     *
     * @param  string $view_name Name of the view (must correspond to filename)
     */
    protected function loadView($view_name) {
        require_once 'views/' . $view_name . '.php';
    }
}
