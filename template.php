<?php

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * Template
 *
 * Provide a template that includes further information and loads menu and view
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */
?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" concent="<?php echo $this->page_description; ?>">
        <meta name="robots" content="<?php echo $this->search_engine; ?>">
        <link rel="icon" href="<?php echo $this->fav_icon; ?>">

        <title><?php echo $this->page_title; ?></title>

        <?php
        // Include Stylesheets
        foreach ($this->stylesheet_path as $stylesheet) : ?>
            <link rel="stylesheet" href="<?php echo $stylesheet; ?>" />
        <?php
        endforeach;

        // Include Javascript files at the top
        foreach ($this->javascript_path_top as $javascript_top) : ?>
            <script type="text/javascript" src="<?php echo $javascript_top; ?>"></script>
        <?php
        endforeach;

        // HTML5 shiv and Respond.js for IE8 support of HTML5 elements and media queries
        ?>
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

    </head>

    <body>
        <?php
        // Only show navigation when logged in
        if ($this->login_status >= 2) :?>
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Switch Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php?site=home"><?php echo $this->site_name; ?></a>
                    </div>

                    <div id="navbar" class="navbar-collapse collapse">
                        <?php
                        // get the menu items
                        $this->showMenu();
                        ?>
                    </div>
                </div>
            </nav>

        <?php
        endif;?>
        <div class="container">
            <?php if ($this->login_status >= 2) :?>
                <div class="error" style="display:none;"></div>
            <?php endif; ?>
            <?php
                // get the main page content
                $this->showView();
            ?>
        </div>

        <?php
        // Include Javascript files at the bottom
        foreach ($this->javascript_path_bottom as $javascript_bottom) : ?>
            <script type="text/javascript" src="<?php echo $javascript_bottom; ?>"></script>
        <?php
        endforeach;
        ?>
    </body>
</html>
