<?php
namespace view;
use library;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * AddTrip
 *
 * Displays the view for adding trips
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */

// Get a list of all members
$user = new library\User();
$members = $user->getUsersList();

// Get the trip object to be able to check if the current user has a car or not
$trip = new library\Trip();
?>
<h4>Add Trip</h4>
<?php if ($trip->getUserCar($_SESSION['user_id']) !== FALSE || $this->login_status >= 3):?>
<form class="form" method="post" action="index.php?action=trip_add">
    <div class="row">


        <div class="col-md-6">
            <div class="form-group">
                <label for="passengers" class="control-label">Mitfahrer</label>
                <select class="form-control select-search" name="passengers" data-placeholder="Select all passengers" multiple required>
                    <?php
                    foreach ($members as $member):
                        echo "<option value='" . $member['id'] . "' >" . $member['mname'] . "</option>";
                    endforeach;
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="date" class="control-label">Datum</label>
                <input class="form-control datetimepicker" type="text" name="date" value="<?php date('d.m.Y', time()); ?>" required/>
            </div>
        </div>

        <?php if ($this->login_status >= 3) :  // Show additional input fields when the login status is sufficient ?>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="driver" class="control-label">Fahrer</label>
                    <select class="form-control select-search" name="driver" data-placeholder="Select driver" required>
                        <?php
                        foreach ($members as $member):
                            echo "<option value='" . $member['id'] . "' >" . $member['mname'] . "</option>";
                        endforeach;
                        ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <div class="checkbox">
                        <label><input type="checkbox" name="driver_pays" checked="checked">Driver pays?</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="checkbox">
                        <label><input type="checkbox" name="calc_price" checked="checked">Calculate price?</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="price" class="control-label">Price</label>
                    <input type="number" name="price" class="form-control" step="0.01">
                </div>
                <input type="hidden" name="admin" value="true" />
            </div>
        <?php endif; ?>

    </div>

    <div class="row">
        <div class="col-md-10"></div>
        <div class="col-md-2">
            <button class="btn btn-sm btn-primary btn-block" type="submit" value="Save" id="saveTrip">
                <span class="glyphicon glyphicon-save"></span>Save</button>
        </div>
    </div>
</form>
<?php else :
    // Fallback if the user does not have a car
    echo "<p>You do not have a car! Please create a car before adding trips.</p>";
endif;
