<?php
namespace view;
use library;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * Admin
 *
 * Show administration view enabling the admin to add deposits and register
 * new users
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */

// Get a list of all users
$user = new library\User();
$members = $user->getUsersList();
$trip = new library\Trip();
$fuels = $trip->getFuelList();

?>
<h4>Administration</h4>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

    <!-- Panel for adding a deposit for a user -->
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="depositHeading">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#depositPanel" aria-expanded="true" aria-controls="depositPanel">
                    Charge Deposit
                </a>
            </h4>
        </div>
        <div id="depositPanel" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="depositHeading">
            <div class="panel-body">
                <form class="form" method="post" action="index.php?action=trip_deposit">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Select User, Charge Deposit and Save.</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="user" class="control-label">Select User</label><br />
                            <select class="form-control select-search" name="user" data-placeholder="Please Select User" required>
                                <?php
                                foreach ($members as $member):
                                    echo "<option value='" . $member['id'] . "' >" . $member['mname'] . "</option>";
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="deposit_amount" class="control-label">New Deposit</label>
                            <input type="number" name="deposit_amount" class="form-control" step="0.01" required>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-2 pull-right">
                            <button class="btn btn-sm btn-primary btn-block" type="submit" id="deposit_save">
                                <span class="glyphicon glyphicon-save"> </span>
                                      Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Panel to register a new user -->
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="registerHeading">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#registerPanel" aria-expanded="true" aria-controls="registerPanel">
                    Add user
                </a>
            </h4>
        </div>
        <div id="registerPanel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="registerHeading">
            <div class="panel-body">
                <form class="form confirm-pw" method="post" action="index.php?action=register">
                    <div class ="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputFirstname" class="control-label">First name</label>
                                <input type='text' name='firstname' id='inputFirstname' class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="inputLastname" class="control-label">Last name</label>
                                <input type='text' name='lastname' id='inputLastname' class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="control-label">E-Mail address</label>
                                <input type="email" name="email" id="inputEmail" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6" >
                            <div class="form-group">
                                <div id="confirm-form">
                                    <label for="password" class="control-label">password</label>
                                    <input type="password" name="password" id="pw-set" class="form-control" required>
                                    <span class="glyphicon glyphicon-remove form-control-feedback" style="display: none"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div id="confirm-form">
                                    <label for="password_confirm" class="control-label">Confirm password</label>
                                    <input type="password" name="password_confirm" id="pw-confirm" class="form-control" required>
                                    <span class="glyphicon glyphicon-remove form-control-feedback" style="display: none"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 pull-right">
                            <button class="btn btn-sm btn-primary btn-block" type="submit" id="registerButton" value="register_submit">
                                <span class="glyphicon glyphicon-save"> </span>
                                  Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Panel for changing fuel prices -->
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="pricesHeading">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#pricesPanel" aria-expanded="true" aria-controls="pricesPanel">
                    Change Prices
                </a>
            </h4>
        </div>
        <div id="pricesPanel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="pricesHeading">
            <div class="panel-body">
                <form class="form" method="post" action="index.php?action=trip_changefuelprice">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Enter new prices and click save.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?php foreach ($fuels as $fuel) : ?>
                                <div class="form-group">
                                    <label for="<?php echo $fuel['ftype'];?>" class="control-label"><?php echo $fuel['ftype'];?></label>
                                    <input type="number" id="<?php echo $fuel['ftype'];?>" name="<?php echo $fuel['id'] . '_' . $fuel['ftype'];?>" class="form-control"
                                           step="0.01"  value="<?php echo number_format($fuel['price'], 2);?>" required>
                                </div>
                            <?php
                                $fuellist[] = $fuel['id'] . '_' . $fuel['ftype'];
                                endforeach; ?>
                            <input type="hidden" name="fuellist" value="<?php echo implode(',', $fuellist); ?>" />
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-2 pull-right">
                            <button class="btn btn-sm btn-primary btn-block" type="submit" id="prices_save">
                                <span class="glyphicon glyphicon-save"> </span>
                                      Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
