<?php
namespace view;
use library;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * AllMembers
 *
 * Display a list of all members and enable the admin to make changes such
 * as approving and deleting users
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */

// Get a list of all members
$user = new library\User();
$members = $user->getUsersList();

foreach ($members as $member) {
    $ids[] =  $member['id'];
}

// Get the full list of members including balances
$trip = new library\Trip();
$balances = $trip->getBalanceList($ids);
?>
<h4>All Members</h4>
<form class="form" method="post" action="index.php?action=user_change">
    <div class="btn-toolbar pull-right" >
        <div class="btn-group" role="group">
            <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <span class="glyphicon glyphicon-asterisk"></span>
                Change Privileges
                <span class="caret"></span>
            </button>
            <button type="submit" class="btn btn-sm btn-default" name="btn_action" value="user_approve">
                <span class="glyphicon glyphicon-ok"></span>
                Approve User
            </button>
            <button type="submit" class="btn btn-sm btn-default" name="btn_action" value="user_delete">
                <span class="glyphicon glyphicon-trash"></span>
                Delete User
            </button>

            <ul class="dropdown-menu">
                <li>
                    <button type="submit" class="btn-link btn-md dropdown-toggle" name="btn_action" value="access_admin">
                        <span class="glyphicon glyphicon-king"></span>
                        Admin
                    </button>
                </li>
                <li>
                    <button type="submit" class="btn-link btn-md dropdown-toggle" name="btn_action" value="access_member">
                        <span class="glyphicon glyphicon-pawn"></span>
                        Member
                    </button>
                </li>
            </ul>
        </div>

    </div>
    <div class="row">
        <table class="table table-hover">
            <thead>
                <tr class="table-header">
                    <th></th>
                    <th>Name</th>
                    <th>EMail</th>
                    <th>Privileges</th>
                    <th>Approval</th>
                    <th style="text-align:right;">Balance</th>
                </tr>
            </thead>
            <tbody class="table-body">
            <?php
            foreach ($balances as $balance):
                echo "<tr id='" . $balance['id'] . "'>";
                echo "  <td><input type='radio' name='user' id='user_" . $balance['id'] . "' "
                                . "value='" . $balance['id'] . "' required /></td>";
                echo "  <td>" . $balance['mname'] . "</td>";
                echo "  <td>" . $balance['email'] . "</td>";
                echo "  <td>" . $balance['access_name'] . "</td>";
                echo "  <td>" . $balance['approved'] . "</td>";
                echo "  <td style='text-align:right;'>" . number_format($balance['balance'], 2, ",", ".") . " &euro;</td>";
                echo "</tr>";
            endforeach;
            ?>
            </tbody>
        </table>
    </div>
</form>
