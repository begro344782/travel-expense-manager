<?php
namespace view;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * ForgotPassword
 *
 * Allows the user to enter their email address and receive an email with
 * password reset info.
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */
?>

<form class="form-reset-mail" method="post" name="forgot_form" id="form-reset-mail" action="index.php?action=user_pwforgot">

    <h2>Reset Password</h2>

    <div class="error" style="display:none;"></div>
    <label for="email">Enter email and wait for an the reset email.</label>
    <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>

    <button class="btn btn-lg btn-primary btn-block" id="resetMailButton" type="submit" value="Reset">Reset Password</button>

    <p><a href="index.php?site=login">Return to login.</a></p>
</form>
