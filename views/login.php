<?php
namespace view;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * Login
 *
 * Show a login form to enable users to login to the application
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */

?>
<form class="form-signin" id="login-form" method="post" name="form_login" action="index.php?action=login">
    <h2 class="form-signin-heading">Please login!</h2>
    <div class="error" style="display:none;"></div>
    <div class="form-group">
        <label class="sr-only" for="login_email" >Email address</label>
        <input class="form-control" type="email" id="login_email" name="login_email"  placeholder="E-Mail Adresse" required autofocus>

        <label class="sr-only" for="input_password" >Password</label>
        <input class="form-control" id="login_password" type="password" name ="login_password" placeholder="Passwort" required>
    </div>

    <div class="form-group">
        <button class="btn btn-lg btn-primary btn-block" id="login_button_send" type="submit" value="login_button_send" >Login</button>
    </div>
    <p>Not registered? <a href="index.php?site=register">Register now!</a></p>
    <p>Forgot password? <a href="index.php?site=forgot-password">Reset password!</a></p>
</form>
