<?php
namespace view;
use library;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * MyTrips
 *
 * Show two lists of the users trips, separated by drives and rides
 *
 * @todo Add sums to the lists
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */

// Get all trips for the current user, separated by drives and rides
$trip = new library\Trip();
$deposits = $trip->getDepositsBalance($_SESSION['user_id']);
?>
<div class="row">
    <h4>Your Deposits</h4>
    <div class="col-md-6">
        <table class="table table-hover">
            <thead>
            <tr class="table-header">
                <th>Date</th>
                <th style='text-align:right;'>Deposit</th>
            </tr>
            </thead>
            <tbody class="table-body">
                <?php
                    if (!empty($deposits)) :
                        foreach ($deposits as $deposit) :
                            echo "<tr>";
                            echo "    <td>" . date('d.m.Y', strtotime($deposit['change_date'])) . "</td>";
                            echo "    <td style='text-align:right;'>" . number_format($deposit['deposit'], 2, ',', '.') . " &euro;</td>";
                            echo "</tr>";
                        endforeach;
                    else:
                        echo "<tr>";
                        echo "    <td colspan=2>No entries!</td>";
                        echo "</tr>";
                    endif;
                ?>
            </tbody>
        </table>
    </div>
</div>
