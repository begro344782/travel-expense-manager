<?php
namespace view;
use library;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * MyTrips
 *
 * Show two lists of the users trips, separated by drives and rides
 *
 * @todo Add sums to the lists
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */

// Get all trips for the current user, separated by drives and rides
$trip = new library\Trip();
$trips = $trip->getTripsByUser($_SESSION['user_id']);

?>
<div class="row">
    <h4>Your Trips as Driver</h4>
    <form class="form" method="post" action="index.php?action=trip_change">
        <div class="row btn-toolbar pull-right" >
            <div class="btn-group" role="group">
                <a href="index.php?site=addtrip" id="tripAdd" class="btn btn-default btn-sm" >
                    <span class="glyphicon glyphicon-plus"></span>
                    Add trip
                </a>
                <button id="tripDelete" class="btn btn-default btn-sm" type="submit" name="btn_action" value="trip_delete">
                    <span class="glyphicon glyphicon-trash"></span>
                    Delete trip
                </button>
            </div>

        </div>
        <table class="table table-hover">
            <thead>
            <tr class="table-header">
                <th></th>
                <th>Date</th>
                <th>Passengers</th>
                <th style='text-align:right;'>Earned</th>
            </tr>
            </thead>
            <tbody class="table-body">
                <?php
                    if (!empty($trips['drives'])) :
                        foreach ($trips['drives'] as $drive) :
                            echo "<tr id=" . $drive['trip_id'] . ">";
                            echo "  <td><input type='radio' name='trip' id='trip_" . $drive['trip_id'] . "' "
                                    . "value='" . $drive['trip_id'] . "' required /></td>";
                            echo "    <td>" . $drive['datum'] . "</td>";
                            echo "    <td>" . $drive['pas_names'] . "</td>";
                            echo "    <td style='text-align:right;'>" . number_format($drive['cost'], 2, ',', '.') . " &euro;</td>";
                            echo "</tr>";
                        endforeach;
                    else:
                        echo "<tr>";
                        echo "    <td colspan=4>No trips!</td>";
                        echo "</tr>";
                    endif;
                ?>
            </tbody>
        </table>
    </form>
</div>
<div class="row">
    <table class="table table-hover">
        <h4>Your Rides</h4>
        <thead>
        <tr class="table-header">
            <th>Date</th>
            <th>Driver</th>
            <th style='text-align:right;'>Cost</th>
        </tr>
        </thead>
        <tbody class="table-body">
            <?php
                if (!empty($trips['rides'])) :
                    foreach ($trips['rides'] as $ride) :
                        echo "<tr id=" . $ride['trip_id'] . ">";
                        echo "    <td>" . $ride['datum'] . "</td>";
                        echo "    <td>" . $ride['driver_name'] . "</td>";
                        echo "    <td style='text-align:right;'>" . number_format($ride['cost'], 2, ',', '.') . " &euro;</td>";
                        echo "</tr>";
                    endforeach;
                else:
                    echo "<tr>";
                    echo "    <td colspan=3>No Rides!</td>";
                    echo "</tr>";
                endif;
            ?>
        </tbody>
    </table>
</div>
