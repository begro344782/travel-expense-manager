<?php
namespace view;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * Profile
 *
 * Show a view to enable the user to change personal information such as the
 * name, email, password and if they have a car.
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */

// Get the current users information
$user = new \library\User();
$user->initializeById($_SESSION['user_id']);

// Get the current users balance from trips
$trip = new \library\Trip();
$balance_inf = $trip->getDepositsBalance($_SESSION['user_id']);
$balance = $balance_inf === FALSE ? 0 : number_format($balance_inf[0]['total'], 2, ',', '.');

// If the current user has a car calculate the price_per_km and set car exists to true
$car = $trip->getUserCar($_SESSION['user_id']);
if (!empty($car)) {
    $car_exists = true;
    $price_per_km = number_format($car['consumpt'] / 100 * $car['fuelprice'], 2, ',', '.');
} else {
    $car_exists = false;
}

// Get a list of all fuels
$fuels = $trip->getFuelList();
?>
<h4>Your Profile</h4>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <!-- Panel for changing the users personal information -->
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="personalHeading">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#personalPanel" aria-expanded="true" aria-controls="personalPanel">
                    Change personal data
                </a>
            </h4>
        </div>
        <div id="personalPanel" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="personalHeading">
            <div class="panel-body">
                <form class="form" method="post" action="index.php?action=user_update">

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="firstname" class="control-label">First name</label>
                            <input type="text" id="firstname" name="firstname" class="form-control" value="<?php echo $user->getFirstname(); ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="lastname" class="control-label">Last name</label>
                            <input type="text" id="lastname" name="lastname" class="form-control" value="<?php echo $user->getLastname(); ?>" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="email" class="control-label">Email address</label>
                            <input type="email" id="email" name="email" class="form-control" value="<?php echo $user->getEmail(); ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="currentDeposit" class="control-label">Your deposit</label>
                            <input type="text" id="currrentDeposit" name="currentDeposit" class="form-control" value="<?php echo $balance; ?>" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 pull-right">
                            <button class="btn btn-sm btn-primary btn-block" type="submit" id="personalButton" value="personal_submit">
                                <span class="glyphicon glyphicon-save"></span>
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Panel for changing the password -->
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="passwordHeading">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#passwordPanel" aria-expanded="true" aria-controls="passwordPanel">
                    Change password
                </a>
            </h4>
        </div>
        <div id="passwordPanel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="passwordHeading">
            <div class="panel-body">
                <form class="form confirm-pw" method="post" action="index.php?action=user_pwupdate">
                    <div class="row">
                        <div class="col-md-6" >
                            <div class="form-group">
                                <div id="confirm-form">
                                    <label for="password" class="control-label">password</label>
                                    <input type="password" id="password" name="password" id="pw-set" class="form-control" required>
                                    <span class="glyphicon glyphicon-remove form-control-feedback" style="display: none"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div id="confirm-form">
                                    <label for="password_confirm" class="control-label">Config password</label>
                                    <input type="password" id="password_confirm" name="password_confirm" id="pw-confirm" class="form-control" required>
                                    <span class="glyphicon glyphicon-remove form-control-feedback" style="display: none"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password_old" class="control-label">Old Password</label>
                                <input type="password" id="password_old" name="password_old" id="pw-confirm" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 pull-right">
                            <button class="btn btn-primary btn-sm btn-block" type="submit" id="pwButton" value="password_submit">
                                <span class="glyphicon glyphicon-edit"></span>
                                Change password
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Panel for adding and changing cars -->
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="carHeading">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#carPanel" aria-expanded="true" aria-controls="carPanel">
                    Add car
                </a>
            </h4>
        </div>
        <div id="carPanel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="carHeading">
            <div class="panel-body">
                <p id="car-settings">
                    <?php if($car_exists) : ?> With the current settings you will receive
                    <span id="getEuro"><?php echo $price_per_km; ?></span>&euro; per kilometer.
                    <?php endif; ?>
                </p>

                <form class="form" method="post" action="index.php?action=change_car">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="consumpt" class="control-label">Liter per 100 km</label>
                            <input type="number" id="consumpt" name="consumpt" class="form-control"  step=0.1 value="<?php echo $car_exists ? $car['consumpt'] : NULL; ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="car-type-select" class="control-label">Select fuel</label>
                            <select class="form-control" id="car-type-select" name="fuel">
                                <?php
                                foreach ($fuels as $fuel):
                                    echo "<option value='" . $fuel['id'] . "' ";
                                    if ($car_exists && intval($fuel['id']) === intval($car['fuelid'])):
                                        echo "selected='selected'";
                                    endif;
                                    echo " >" . $fuel['ftype'] . "</option>";
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="seats" class="control-label">Number of seats</label>
                            <input type="number" id="seats" name="seats" class="form-control" min="1" max="9" value="<?php echo $car_exists ? $car['seats']: NULL; ?>" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class ="col-md-4 pull-right">
                            <div class="col-md-6">
                                <button class="btn btn-warning btn-sm btn-block" type="submit" id="saveButton"name="btn_action" value="delete">
                                    <span class="glyphicon glyphicon-remove"></span>
                                    Delete
                                </button>
                            </div>

                            <div class="col-md-6">
                                <button class="btn btn-primary btn-sm btn-block" type="submit" id="deleteButton"name="btn_action" value="save">
                                    <span class="glyphicon glyphicon-save"></span>
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
