<?php
namespace view;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * Register
 *
 * Show a view to enable users to register
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */
?>

<form class="form-register confirm-pw" id="register-form" method="post" name="form_register" action="index.php?action=register">
    <h2>Register</h2>

    <div class="error" style="display:none;"></div>
    <div class="form-group">
        <label for="inputFirstname" class="sr-only">Frrst name</label>
        <input type='text' name='firstname' id='inputFirstname' class="form-control" placeholder="Vorname" required>

        <label for="inputLastname" class="sr-only">Last name</label>
        <input type='text' name='lastname' id='inputLastname' class="form-control" placeholder="Nachname" required>

        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="E-Mail Adresse" required>
    </div>
    <div class="form-group">
        <div id="confirm-form">
            <label for="password" class="sr-only">Password</label>
            <input type="password" name="password" id="pw-set" class="form-control" placeholder="Passwort" required>
            <span class="glyphicon glyphicon-remove form-control-feedback" style="display: none"></span>
        </div>
        <div id="confirm-form">
            <label for="password_confirm" class="sr-only">Confirm password</label>
            <input type="password" name="password_confirm" id="pw-confirm" class="form-control" placeholder="Passwort bestätigen" required>
            <span class="glyphicon glyphicon-remove form-control-feedback" style="display: none"></span>
        </div>
    </div>

    <button class="btn btn-lg btn-primary btn-block" type="submit" id="registerButton" value="register_submit">Register</button>
    <p><a href="index.php?site=login">Return to login.</a>.</p>
</form>
