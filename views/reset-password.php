<?php
namespace view;
use library;

// Only allow access via index.php
defined('_MAINEXEC') or die;

/**
 * ResetPassword
 *
 * If the user forgot his password he receives an email with a link to this page
 * where he can set a new password.
 *
 * @package TravelExpenseManager
 * @author Benedikt Grosch
 * @copyright Copyright (C) 2017 Benedikt Grosch.
 */
?>



<form class="form-reset confirm-pw" method="post" name="reset_form" id="reset_form" action="index.php?action=user_pwreset">

    <h2 class="form-signin-heading">Reset Password.</h2>
    <div class="error" style="display:none;"></div>

    <div class="form-group">
        <div id="confirm-form">
            <label for="password" class="sr-only">Password</label>
            <input type="password" name="password" class="form-control" placeholder="Password" required>
            <span class="glyphicon glyphicon-remove form-control-feedback" style="display: none"></span>
        </div>
        <div id="confirm-form">
            <label for="password_confirm" class="sr-only">Confirm Password</label>
            <input type="password" name="password_confirm" class="form-control" placeholder="Confirm Password" required>
            <span class="glyphicon glyphicon-remove form-control-feedback" style="display: none"></span>
        </div>
    </div>
    <input type="hidden" name ="email" readonly required value="<?php echo library\Input::getEmail('get', 'email'); ?>">
    <input type="hidden" name ="id"  readonly required value="<?php   echo library\Input::getInt('get', 'id'); ?>">
    <input type="hidden" name ="token" readonly required value="<?php echo library\Input::getString('get', 'token'); ?>">

    <button class="btn btn-lg btn-primary btn-block" id="resetPasswordButton" type="submit" value="reset_submit">Submit</button>
    <p><a href="index.php?site=login">Return to login.</a></p>
</form>
